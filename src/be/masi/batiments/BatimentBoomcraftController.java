package be.masi.batiments;

import be.masi.httpconnection.ConnectionHowob;
import be.masi.hud.Hud;
import be.masi.map.Map;
import org.lwjgl.Sys;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Xavie on 16-11-15.
 */
public class BatimentBoomcraftController extends BatimentsController {

    List<Batiments>  batimentsBoomcrafts = new ArrayList<Batiments>();
    List<Batiments> batimentsBoomcraftsTemp = new ArrayList<Batiments>();

    public BatimentBoomcraftController(Map map, Hud hud) {
        super(map, hud);

    }

    @Override
    public void init() throws SlickException {


        //CreateBatiments();
    }

    @Override
    public void render(Graphics g) throws SlickException {
        for (Batiments b : batimentsBoomcrafts) {
            b.render(g);
        }
    }

    public synchronized void CreateBatiments(String map) {
        for (int i = 0; i < batimentsBoomcrafts.size(); i++) {
            try {
                batimentsBoomcrafts.get(i).init();
            } catch (SlickException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0 ; i< batimentsBoomcrafts.size(); i++)
        {
            generateBatiments(i, map);
        }
    }

    public synchronized void generateBatiments(int index, String map) {
        super.generateBatiments(index, batimentsBoomcrafts, map);
    }

    public HashMap<Integer, Batiments> isCollision(int xPlayer, int yPlayer) {
        return super.isCollision(batimentsBoomcrafts, xPlayer, yPlayer);
    }

    public void setBatimentsBoomcrafts(List<Batiments> batimentsBoomcrafts) {
        this.batimentsBoomcrafts = batimentsBoomcrafts;
    }

    public synchronized List<Integer> CompareList() {
        List<Integer> diff = new ArrayList<Integer>();

        int indexBatimentAEffacer = 0;
        int indexBatimentAAjouter = 0;

        try {
            if (batimentsBoomcrafts != batimentsBoomcraftsTemp) {
                if(batimentsBoomcrafts != null && batimentsBoomcraftsTemp != null) {
                    if(batimentsBoomcrafts.size() > 0) {
                        for (Batiments batiments : batimentsBoomcrafts) {
                            indexBatimentAEffacer = batimentsBoomcrafts.indexOf(batiments);
                            if (batimentsBoomcraftsTemp.size() > 0) {
                                for (Batiments batimentsTemp : batimentsBoomcraftsTemp) {
                                    if (batiments.id == batimentsTemp.id) {
                                        batiments.vieNow = batimentsTemp.vieNow;
                                        batiments.vieMax = batimentsTemp.vieMax;
                                        batiments.defense = batimentsTemp.defense;
                                        indexBatimentAEffacer = 0;
                                    }
                                }
                                if (indexBatimentAEffacer > 0) {
                                    batimentsBoomcrafts.remove(indexBatimentAEffacer);
                                }
                            }
                        }
                        } else batimentsBoomcrafts.clear();
                    }

                if(batimentsBoomcraftsTemp.size() > 0) {
                    for (Batiments batimentsTemp : batimentsBoomcraftsTemp) {
                        indexBatimentAAjouter = batimentsBoomcraftsTemp.indexOf(batimentsTemp);
                        for (Batiments batiments : batimentsBoomcrafts) {
                            if (batimentsTemp.id == batiments.id) {
                                indexBatimentAAjouter = 0;
                            }
                        }
                        if (indexBatimentAAjouter > 0) {
                            batimentsBoomcrafts.add(batimentsBoomcraftsTemp.get(indexBatimentAAjouter));
                            diff.add(indexBatimentAAjouter);
                        }
                    }
                }
                }
        }catch (Exception e)
        {
            System.out.println("Problème compare");
        }
        return diff;

    }

    public List<Batiments> getBatimentsBoomcraftsTemp() {
        return batimentsBoomcraftsTemp;
    }

    public void setBatimentsBoomcraftsTemp(List<Batiments> batimentsBoomcraftsTemp) {
        this.batimentsBoomcraftsTemp = batimentsBoomcraftsTemp;
    }

    public List<Batiments> getBatimentsBoomcrafts() {
        return batimentsBoomcrafts;
    }
}
