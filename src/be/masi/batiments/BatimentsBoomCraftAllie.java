package be.masi.batiments;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by Xavie on 16-11-15.
 */
public class BatimentsBoomCraftAllie extends BatimentsBoomcraft {

    public BatimentsBoomCraftAllie(int id, String name, int teamid, int defense,int maxlife, int currentlife) {
        super();
        super.id = id;
        super.name = name;
        super.teamId = teamid;
        super.vieMax = maxlife;
        super.defense = defense;
        super.vieNow = currentlife;
    }

    @Override
    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/batiments/house1.png", 228, 170);
        super.animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
    }
}
