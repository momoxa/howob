package be.masi.batiments;

import org.newdawn.slick.SlickException;

/**
 * Created by Xavie on 16-11-15.
 */
public abstract class BatimentsBoomcraft extends Batiments{
    public BatimentsBoomcraft() {
        super();
    }

    @Override
    public abstract void init() throws SlickException ;

}
