package be.masi.batiments;

import be.masi.hud.Hud;
import be.masi.map.Map;
import org.lwjgl.Sys;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Xavie on 16-11-15.
 */
public abstract class BatimentsController {

    protected Map map;
    protected Hud hud;
    public BatimentsController(Map map, Hud hud)
    {
        this.map = map;
        this.hud = hud;
    }
    public abstract void init()  throws SlickException;
    public abstract void render(Graphics g) throws SlickException;
    public void update() {}


     public synchronized HashMap<Integer, Batiments> isCollision(List<Batiments> batiments, int xPlayer, int yPlayer)
     {
         HashMap<Integer, Batiments> batimentsId = new HashMap<Integer, Batiments>();
        for (int i = 0; i< batiments.size(); i++)
        {
            float xbatiments = batiments.get(i).getX ();
            float ybatiments = batiments.get(i).getY();

            if (((xPlayer <= xbatiments + batiments.get(i).getAnimations()[0].getWidth()) && (xPlayer >= xbatiments)) &&
                    (( yPlayer <= ybatiments + batiments.get(i).getAnimations()[0].getHeight()) && (yPlayer >= ybatiments)))
            {
                 batimentsId.put(i, batiments.get(i));
                return batimentsId;
            }
        }
        return null;
     }

    public synchronized void generateBatiments(int index, List<Batiments> batiments, String mapString)
    {
        Random r = new Random();
        Map map = new Map();
        map.changeMap(mapString);
        int x = r.nextInt((map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth()) - batiments.get(index).getAnimations()[0].getWidth());
        int y = r.nextInt((map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight()) - batiments.get(index).getAnimations()[0].getHeight());
        boolean collison = false;

        for(int xb = x; xb< x + batiments.get(index).getAnimations()[0].getWidth(); xb++) {
            for (int yb = y; yb < y +  batiments.get(index).getAnimations()[0].getHeight(); yb++) {
                if (map.isCollision(xb, yb) || isCollision(batiments,xb, yb) != null) {
                    collison = true;
                    break;
                }
            }
        }
        if(collison) {
            generateBatiments(index, batiments, mapString);
        } else {
            batiments.get(index).setX(x);
            batiments.get(index).setY(y);

        }
    }
}
