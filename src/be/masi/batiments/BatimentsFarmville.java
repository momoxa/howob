package be.masi.batiments;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by Xavie on 16-11-15.
 */
public  class BatimentsFarmville extends Batiments {

    public BatimentsFarmville()
    {
    }
    public BatimentsFarmville(int id) {
        super();
        super.id = id;
    }

    @Override
    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/batiments/house2.png", 239, 170);
        super.animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
    }


}
