package be.masi.batiments;

import be.masi.hud.Hud;
import be.masi.map.Map;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Xavie on 16-11-15.
 */
public class BatimentsFarmvilleController extends BatimentsController {
    List<Batiments> batimentsFarmVilles = new ArrayList<Batiments>();

    public BatimentsFarmvilleController(Map map, Hud hud) {
        super(map, hud);
    }

    @Override
    public void init() throws SlickException {
        //CreateBatiments();
    }

    @Override
    public void render(Graphics g) throws SlickException {
        for(Batiments b : batimentsFarmVilles) {
            b.render(g);
        }
    }

    public void CreateBatiments(String map)
    {
        for (int i = 0; i < batimentsFarmVilles.size(); i++) {
            try {
                batimentsFarmVilles.get(i).init();
            } catch (SlickException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0 ; i< batimentsFarmVilles.size(); i++)
        {
            generateBatiments(i, map);
        }
    }
    private void generateBatiments(int index, String map)
    {
       super.generateBatiments(index, batimentsFarmVilles, map);
    }

    public HashMap<Integer, Batiments> isCollision( int xPlayer, int yPlayer) {
        return super.isCollision(batimentsFarmVilles, xPlayer, yPlayer);
    }

    public List<Batiments> getBatimentsFarmVilles() {
        return batimentsFarmVilles;
    }

    public void setBatimentsFarmVilles(List<Batiments> batimentsFarmVilles) {
        this.batimentsFarmVilles = batimentsFarmVilles;
    }
}
