package be.masi.batiments;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by Xavie on 17-11-15.
 */
public class VillageFarmVilleAllie extends VillageFarmville {

    public VillageFarmVilleAllie(String pseudo, int teamId, int vieMax, int vieNow) {
        super();
        super.pseudo = pseudo;
        super.teamId = teamId;
        super.vieMax = vieMax;
        super.vieNow = vieNow;
    }

    @Override
    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/batiments/castle1.png", 157, 250);
        super.animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
    }
}
