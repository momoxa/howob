package be.masi.batiments;

import be.masi.hud.Hud;
import be.masi.map.Map;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Xavie on 17-11-15.
 */
public class VillageFarmVilleController extends BatimentsController{
    List<Batiments> villagesFarmVilles = new ArrayList<Batiments>();
    List<Batiments> villagesFarmVillesTemp = new ArrayList<Batiments>();

    public VillageFarmVilleController(Map map, Hud hud) {
        super(map, hud);
    }

    @Override
    public void init() throws SlickException {

        //CreateBatiments();
    }

    @Override
    public void render(Graphics g) throws SlickException {
        for(Batiments b : villagesFarmVilles) {
            b.render(g);
        }
    }

    public void CreateBatiments(String map)
    {
        for (int i = 0; i < villagesFarmVilles.size(); i++) {
            try {
                villagesFarmVilles.get(i).init();
            } catch (SlickException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0 ; i< villagesFarmVilles.size(); i++)
        {
            generateBatiments(i, map);
        }
    }
    public void generateBatiments(int index, String map)
    {
        super.generateBatiments(index, villagesFarmVilles, map);
    }

    public HashMap<Integer, Batiments> isCollision( int xPlayer, int yPlayer) {
        return super.isCollision(villagesFarmVilles, xPlayer, yPlayer);
    }

    public List<Batiments> getVillagesFarmVilles() {
        return villagesFarmVilles;
    }

    public void setVillagesFarmVilles(List<Batiments> villagesFarmVilles) {
        this.villagesFarmVilles = villagesFarmVilles;
    }

    public List<Batiments> getVillagesFarmVillesTemp() {
        return villagesFarmVillesTemp;
    }

    public void setVillagesFarmVillesTemp(List<Batiments> villagesFarmVillesTemp) {
        this.villagesFarmVillesTemp = villagesFarmVillesTemp;
    }
    public synchronized List<Integer> CompareList() {
        List<Integer> diff = new ArrayList<Integer>();

        int indexBatimentAEffacer = 0;
        int indexBatimentAAjouter = 0;


        if (villagesFarmVilles != villagesFarmVillesTemp) {
            for(Batiments batiments : villagesFarmVilles) {
                indexBatimentAEffacer = villagesFarmVilles.indexOf(batiments);
                for(Batiments batimentsTemp : villagesFarmVillesTemp) {
                    if(batiments.id == batimentsTemp.id){
                        batiments.vieNow = batimentsTemp.vieNow;
                        batiments.vieMax = batimentsTemp.vieMax;
                        batiments.defense = batimentsTemp.defense;
                        indexBatimentAEffacer = 0;
                    }
                }
                if(indexBatimentAEffacer > 0){
                    villagesFarmVilles.remove(indexBatimentAEffacer);
                }
            }

            for (Batiments batimentsTemp : villagesFarmVillesTemp) {
                indexBatimentAAjouter = villagesFarmVillesTemp.indexOf(batimentsTemp);
                for (Batiments batiments : villagesFarmVilles) {
                    if (batimentsTemp.id == batiments.id) {
                        indexBatimentAAjouter = 0;
                    }
                }
                if(indexBatimentAAjouter > 0)
                {
                    villagesFarmVilles.add(villagesFarmVillesTemp.get(indexBatimentAAjouter));
                    diff.add(indexBatimentAAjouter);
                }
            }
        }
        return diff;
    }

}
