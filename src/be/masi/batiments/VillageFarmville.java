package be.masi.batiments;

import org.newdawn.slick.SlickException;

/**
 * Created by Xavie on 17-11-15.
 */
public abstract class VillageFarmville extends BatimentsFarmville {
    @Override
    public abstract void init() throws SlickException ;
}
