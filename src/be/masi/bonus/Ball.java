package be.masi.bonus;

import org.newdawn.slick.*;


/**
 * Created by Ghost_Wanted on 03/11/15.
 */
public abstract class Ball {

    private float x, y;
    protected Animation[] animations = new Animation[1];

    public Ball(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    protected Animation loadAnimation(SpriteSheet spriteSheet, int startX, int endX, int y) {
        Animation animation = new Animation();
        for (int x = startX; x < endX; x++) {
            animation.addFrame(spriteSheet.getSprite(x, y), 500);
        }
        return animation;
    }

    public abstract void init() throws SlickException;

    public void update() {

    }

    public void render(Graphics g){
        g.drawAnimation(animations[0], x, y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }


    public abstract int getMANIA();
    public abstract int getEXPERIENCE();
    public abstract int getWOOD();
    public abstract int getSTEEL();
    public abstract int getPOTION();
    public abstract int getLIFE();
}
