package be.masi.bonus;

import be.masi.hud.Hud;
import be.masi.map.Map;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Ghost_Wanted on 03/11/15.
 */
public class BallController {

    private Map map;
    private Hud hud;
    List<Ball> ballList = new ArrayList<Ball>();

    public void init() throws SlickException {
        ballList.add(new GreenBall(10, 10)); // + 10 de Mania
        ballList.add(new GreenBall(10, 10)); // + 10 de Mania
        ballList.add(new MallowBall(10, 10)); // + 1 d'expérience
        ballList.add(new YellowBall(10, 10)); // + 1 ressources
        ballList.add(new heart(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new heart(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Potion(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Potion(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Potion(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Steel(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Steel(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Steel(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Steel(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Wood(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Wood(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Wood(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Wood(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience
        ballList.add(new Wood(10, 10)); // + 25 de Mania + 1 ressources + 1 d'éxpérience


        for(int i = 0; i< ballList.size(); i++)
        {
            generateBalls(i);
            ballList.get(i).init();
        }

    }

    public BallController(Map map, Hud hud)
    {
        this.map = map;
        this.hud = hud;
    }

    public void generateBalls(int index)
    {
        Random r = new Random();
        int x = r.nextInt(map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth());
        int y = r.nextInt(map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight());
        if(map.isCollision(x, y))
        {
            generateBalls(index);
        }
        else {
            ballList.get(index).setX(x);
            ballList.get(index).setY(y);
            hud.updateBonus(ballList);
        }

    }

    public int[] isCollision(int x, int y)
    {
        int[] tabRetour = new  int [6];
        for (int i = 0; i< ballList.size(); i++)
        {
            float xball = ballList.get(i).getX();
            float yball = ballList.get(i).getY();

            if (((x <= xball + ballList.get(i).animations[0].getWidth()) && (x >= xball)) &&
                    (( y <= yball  + ballList.get(i).animations[0].getHeight()) && (y >= yball)))
            {
                generateBalls(i);
                tabRetour[0] = ballList.get(i).getMANIA();
                tabRetour[1] = ballList.get(i).getEXPERIENCE();
                tabRetour[2] = ballList.get(i).getPOTION();
                tabRetour[3] = ballList.get(i).getSTEEL();
                tabRetour[4] = ballList.get(i).getWOOD();
                tabRetour[5] = ballList.get(i).getLIFE();
            }
        }
        return tabRetour;
    }

    public void render(Graphics g)
    {
        for(Ball b : ballList)
            b.render(g);
    }

    public void update()
    {
        for(Ball b : ballList)
            b.update();
    }
}
