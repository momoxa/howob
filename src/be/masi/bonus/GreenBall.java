package be.masi.bonus;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by Ghost_Wanted on 03/11/15.
 */
public class GreenBall extends Ball {

    private final int MANIA = 0;
    private final int EXPERIENCE = 1;
    private final int WOOD = 0;
    private final int STEEL = 0;
    private final int POTION = 0;
    private final int LIFE = 0;



    public GreenBall(int x, int y)
    {
        super(x, y);
    }

    @Override
    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/bonus/balls.png", 32, 32);
        super.animations[0] = loadAnimation(spriteSheet, 0, 6, 2);
    }

    @Override
    public int getMANIA() {
        return MANIA;
    }

    @Override
    public int getEXPERIENCE() {
        return EXPERIENCE;
    }
    @Override
    public int getWOOD() {
        return WOOD;
    }
    @Override
    public int getSTEEL() {
        return STEEL;
    }
    @Override
    public int getPOTION() {
        return POTION;
    }
    @Override
    public int getLIFE() {
        return LIFE;
    }
}
