package be.masi.bonus;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Created by Ghost_Wanted on 03/11/15.
 */
public class Wood extends Ball {

    private final int MANIA = 0;
    private final int EXPERIENCE = 0;
    private final int WOOD = 1;
    private final int STEEL = 0;
    private final int POTION = 0;
    private final int LIFE = 0;

    public Wood(int x, int y)
    {
        super(x, y);
    }

    @Override
    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/bonus/wood.png", 32, 23);

        super.animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
    }

    @Override
    public int getMANIA() {
        return MANIA;
    }

    @Override
    public int getEXPERIENCE() {
        return EXPERIENCE;
    }
    @Override
    public int getWOOD() {
        return WOOD;
    }
    @Override
    public int getSTEEL() {
        return STEEL;
    }
    @Override
    public int getPOTION() {
        return POTION;
    }
    @Override
    public int getLIFE() {
        return LIFE;
    }
}
