package be.masi.camera;

import be.masi.player.Player;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;


/**
 * @author Mohamed Amine Hosni, Xavier Conrath
 * @version 1.0
 * Class permettant d'avoir la caméra suivant le joueur à l'écran
 */
public class Camera {

    private Player player;
    private float xCamera, yCamera;

    public Camera(Player player) {
        this.player = player;
        this.xCamera = player.getX();
        this.yCamera = player.getY();
    }

    public void render(GameContainer container, Graphics g) {
        if(this.xCamera < container.getWidth() /2) {
            xCamera = container.getWidth() / 2;
        }
        if (this.yCamera < container.getHeight() / 2){
            yCamera = container.getHeight() /2;
        }

        int width = (player.getMap().getTiledMap().getWidth() * player.getMap().getTiledMap().getTileWidth());
        int height =  (player.getMap().getTiledMap().getHeight() * player.getMap().getTiledMap().getTileHeight());

        if(yCamera > height - container.getHeight() /2) {
            yCamera = height - container.getHeight() /2;
        }
        if(xCamera > width - container.getWidth() /2 ) {
            xCamera = width - container.getWidth() /2;
        }

        g.translate( container.getWidth() / 2 - (int) this.xCamera, container.getHeight() / 2 - (int) this.yCamera);
    }

    public void update(GameContainer container) {
        int w = container.getWidth() / 4;
        if (this.player.getX() > this.xCamera + w) {
            this.xCamera = this.player.getX() - w;
        } else if (this.player.getX() < this.xCamera - w) {
            this.xCamera = this.player.getX() + w;
        }
        int h = container.getHeight() / 4;
        if (this.player.getY() > this.yCamera + h) {
            this.yCamera = this.player.getY() - h;
        } else if (this.player.getY() < this.yCamera - h) {
            this.yCamera = this.player.getY() + h;
        }

    }

}
