package be.masi.connectiongui;

import be.masi.httpconnection.Connection;
import be.masi.httpconnection.ConnectionHowob;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;


/**
 * Created by Ghost_Wanted on 05/11/15.
 */
public class LoginFrame extends Application {

    @Override
    public void start(final Stage primaryStage) {

        StackPane root = new StackPane();
        final Scene scene = new Scene(root, 800, 600);

        final TextField loginTextField = new TextField();
        final PasswordField passwordField = new PasswordField();

        loginTextField.setPromptText("Login");
        passwordField.setPromptText("Password");

        final Button btnLogin = new Button();
        btnLogin.setText("Se connecter");
        btnLogin.setDefaultButton(true);

        final Button btnRetour = new Button();

        btnRetour.setText("Annuler");

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                btnLogin.requestFocus();
            }
        });

        btnRetour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new PrincipalFrame().start(primaryStage);
            }

        });

        btnLogin.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                final int[] id = {0};

                Task<Integer> task = new Task<Integer>() {
                    @Override protected Integer call() throws Exception {

                        scene.setCursor(Cursor.WAIT);

                        try {
                            id[0] = Connection.getInstance().login(loginTextField.getText(), passwordField.getText());
                        } catch(Exception e) {

                            e.printStackTrace();
                        }
                        return id[0];
                    }

                    @Override protected void succeeded() {
                        super.succeeded();
                        scene.setCursor(Cursor.DEFAULT);

                        if(id[0] > 0)
                        {
                            ConnectionHowob.getInstance().setLogin(loginTextField.getText());
                            new TeamFrame().start(primaryStage);
                        } else {
                            showAlert("Erreur...", "Problème de connexion", "Veuillez vérifier votre login ou mot de passe");
                        }

                        updateMessage("Done!");
                    }

                    @Override protected void cancelled() {
                        super.cancelled();
                        scene.setCursor(Cursor.DEFAULT);
                        updateMessage("Cancelled!");
                    }

                    @Override protected void failed() {
                        super.failed();

                        scene.setCursor(Cursor.DEFAULT);

                        showAlert("Erreur...", "Probléme de connexion", "Veuillez réessayer plus tard");

                        updateMessage("Failed!");
                    }

                };

                Thread th = new Thread(task);
                th.setDaemon(true);
                th.start();
            }
        });


        TilePane tilePanePrincipal = new TilePane(Orientation.VERTICAL);
        tilePanePrincipal.setPadding(new Insets(220, 5, 5, 300));
        tilePanePrincipal.setVgap(1.0);

        TilePane tilePaneChamps = new TilePane(Orientation.VERTICAL);
        tilePaneChamps.setVgap(10.0);
        tilePaneChamps.getChildren().addAll(loginTextField, passwordField);

        TilePane tilePaneButton = new TilePane(Orientation.VERTICAL);
        tilePaneButton.setPadding(new Insets(0, 0, 0, 30));
        tilePaneButton.setVgap(1.0);
        tilePaneButton.getChildren().add(btnLogin);
        tilePaneButton.getChildren().add(btnRetour);

        tilePanePrincipal.getChildren().add(tilePaneChamps);
        tilePanePrincipal.getChildren().add(tilePaneButton);

        // set background
        BackgroundImage myBI= new BackgroundImage(new Image("resources/background/forest.png", 800, 600, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        root.setBackground(new Background(myBI));
        root.getChildren().add(tilePanePrincipal);


        primaryStage.setTitle("HoWoB - Login");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void showAlert(String erreur, String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(erreur);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

}
