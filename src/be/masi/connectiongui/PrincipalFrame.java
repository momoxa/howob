package be.masi.connectiongui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * Created by Ghost_Wanted on 05/11/15.
 */
public class PrincipalFrame extends Application {


    public void launchApplication(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        Button btnRegistration = new Button();
        Button btnLogin = new Button();

        btnRegistration.setText("Créer un compte");
        btnLogin.setText("Se connecter");

        btnLogin.setDefaultButton(true);

        btnRegistration.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                new RegistrationFrame().start(primaryStage);
            }
        });

        btnLogin.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                new LoginFrame().start(primaryStage);
            }
        });

        TilePane tileButtons = new TilePane(Orientation.HORIZONTAL);
        tileButtons.setPadding(new Insets(500, 160, 60, 185));
        tileButtons.setHgap(150.0);
        tileButtons.setVgap(150.0);
        tileButtons.getChildren().addAll(btnLogin, btnRegistration);


        // TEXT
        InnerShadow is = new InnerShadow();
        is.setOffsetX(4.0f);
        is.setOffsetY(4.0f);

        Text title = new Text();
        title.setEffect(is);
        title.setX(100);
        title.setY(60);
        title.setText("HoWob");
        title.setFill(Color.ORANGE);
        title.setFont(Font.font("Calibri", FontWeight.BOLD, 180));

        // set background
        BackgroundImage myBI= new BackgroundImage(new Image("resources/background/forest.png", 800, 600, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        StackPane root = new StackPane();

        root.setBackground(new Background(myBI));
        root.getChildren().add(title);
        root.getChildren().add(tileButtons);


        Scene scene = new Scene(root, 800, 600);

        primaryStage.setTitle("HoWoB");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

}
