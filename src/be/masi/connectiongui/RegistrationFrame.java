package be.masi.connectiongui;

import be.masi.httpconnection.Connection;
import be.masi.httpconnection.ConnectionHowob;
import be.masi.httpconnection.ConnectionVeggieCrush;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.lwjgl.Sys;

/**
 * Created by Ghost_Wanted on 05/11/15.
 */
public class RegistrationFrame extends Application {

    public RegistrationFrame()
    {

    }

    @Override
    public void start(final Stage primaryStage) {

        final TextField loginTextField = new TextField();
        final PasswordField passwordField = new PasswordField();
        final TextField emailTextField = new TextField();
        final TextField firstnameTextField = new TextField();
        final TextField lastnameTextField = new TextField();

        loginTextField.setPromptText("Login");
        passwordField.setPromptText("Password");
        emailTextField.setPromptText("Email");
        firstnameTextField.setPromptText("Firstname");
        lastnameTextField.setPromptText("Lastname");

        StackPane root = new StackPane();
        final Scene scene = new Scene(root, 800, 600);

        final Button btnRegistration = new Button();

        btnRegistration.setText("Créer un compte");
        btnRegistration.setDefaultButton(true);

        final Button btnRetour = new Button();

        btnRetour.setText("Annuler");

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                btnRegistration.requestFocus();
            }
        });

        btnRetour.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new PrincipalFrame().start(primaryStage);
            }

        });

        btnRegistration.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                final int[] id = {0};

                Task<Integer> task = new Task<Integer>() {
                    @Override protected Integer call() throws Exception {

                        scene.setCursor(Cursor.WAIT);

                        try {

                            if(Connection.getInstance().checkLogin(loginTextField.getText())) {
                                id[0] = -3;
                            }
                            else {
                                id[0] = ConnectionHowob.getInstance().Registration(loginTextField.getText(), passwordField.getText(),
                                        emailTextField.getText(), firstnameTextField.getText(), lastnameTextField.getText());
                                ConnectionVeggieCrush.getInstance().registration(loginTextField.getText(), passwordField.getText());
                            }
                        } catch(Exception e)
                        {
                            e.printStackTrace();
                        }

                        return id[0];
                    }

                    @Override protected void succeeded() {
                        super.succeeded();
                        scene.setCursor(Cursor.DEFAULT);

                        System.out.print("ID" + id[0]);
                        if(id[0] > 0)
                        {
                            new TeamFrame().start(primaryStage);
                        }
                        else if(id[0] == -1){
                            showAlert("Erreur...", "Problème de champs", "Veuillez vérifier les champs");
                        } else if (id[0] == -2){
                            showAlert("Erreur...", "Problème de login", "Ce login est déjà attribué !");
                        }
                        else if(id[0] == -3) {
                            showAlert("Erreur...", "Problème de login", "Ce login est déjà attribué dans le domaine Boomcraft!");
                        }
                        else showAlert("Erreur...", "erreur", "erreur...");

                        updateMessage("Done!");
                    }

                    @Override protected void cancelled() {
                        super.cancelled();
                        scene.setCursor(Cursor.DEFAULT);
                        updateMessage("Cancelled!");
                    }

                    @Override protected void failed() {
                        super.failed();

                        scene.setCursor(Cursor.DEFAULT);

                        showAlert("Erreur...", "Probléme de connexion", "Veuillez réessayer plus tard");

                        updateMessage("Failed!");
                    }

                };

                Thread th = new Thread(task);
                th.setDaemon(true);
                th.start();
            }
        });

        TilePane tilePanePrincipal = new TilePane(Orientation.VERTICAL);
        tilePanePrincipal.setPadding(new Insets(180, 10, 20, 300));
        tilePanePrincipal.setVgap(40.0);

        TilePane tilePaneChamps = new TilePane(Orientation.VERTICAL);
        tilePaneChamps.setVgap(10.0);

        tilePaneChamps.getChildren().addAll(loginTextField, passwordField, emailTextField,
                firstnameTextField, lastnameTextField);

        TilePane tilePaneButton = new TilePane(Orientation.VERTICAL);
        tilePaneButton.setPadding(new Insets(0, 0, 0, 22));
        tilePaneButton.setVgap(8.0);
        tilePaneButton.getChildren().add(btnRegistration);
        tilePaneButton.getChildren().add(btnRetour);

        tilePanePrincipal.getChildren().add(tilePaneChamps);
        tilePanePrincipal.getChildren().add(tilePaneButton);


        // set background
        BackgroundImage myBI= new BackgroundImage(new Image("resources/background/forest.png", 800, 600, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        root.setBackground(new Background(myBI));
        root.getChildren().add(tilePanePrincipal);
        //root.getChildren().add(gridPane);



        primaryStage.setTitle("HoWoB - Registration");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void showAlert(String erreur, String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(erreur);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

}
