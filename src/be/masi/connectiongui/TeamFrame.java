package be.masi.connectiongui;

import be.masi.httpconnection.ConnectionHowob;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * Created by Ghost_Wanted on 05/11/15.
 */
public class TeamFrame extends Application {

    @Override
    public void start(final Stage primaryStage) {
        Button btnRed = new Button();
        Button btnBlue = new Button();

        btnRed.setText("Rouge");
        btnRed.setStyle("-fx-font: 30 calibri; -fx-base: #C24641;");
        btnRed.setPrefSize(150, 100);

        btnBlue.setText("Bleu");
        btnBlue.setStyle("-fx-font: 30 calibri; -fx-base: #1E90FF;");
        btnBlue.setPrefSize(150, 100);
        btnBlue.setDefaultButton(true);

        btnRed.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                ConnectionHowob.getInstance().setTeamId(2);
                try {
                    primaryStage.close();
                    //new AppGameContainer(new StateGame(), 800, 600, false).start();
                    //Connection.getInstance().save(Player.life, Player.experience,Player.mania, Player.potion, Player.wood, Player.steel);
                    //Connection.getInstance().Logout();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnBlue.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                ConnectionHowob.getInstance().setTeamId(1);
                try {
                    primaryStage.close();
                    //new AppGameContainer(new StateGame(), 800, 600, false).start();
                    //Connection.getInstance().save(Player.life, Player.experience,Player.mania,  Player.potion, Player.wood, Player.steel);
                    //Connection.getInstance().Logout();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        TilePane tileButtons = new TilePane(Orientation.HORIZONTAL);
        tileButtons.setPadding(new Insets(260, 160, 60, 165));
        tileButtons.setHgap(150.0);
        tileButtons.setVgap(150.0);
        tileButtons.getChildren().addAll(btnRed, btnBlue);

        StackPane root = new StackPane();

        // set background
        BackgroundImage myBI= new BackgroundImage(new Image("resources/background/forest.png", 800, 600, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        root.setBackground(new Background(myBI));
        root.getChildren().add(tileButtons);

        Scene scene = new Scene(root, 800, 600);

        primaryStage.setTitle("HoWoB - Choose your team");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
