package be.masi.core;
import be.masi.connectiongui.PrincipalFrame;
import be.masi.httpconnection.ConnectionHowob;
import be.masi.httpconnection.ConnectionVeggieCrush;
import be.masi.player.Player;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 * @author Mohamed Amine Hosni, Xavier Conrath
 * @version 1.0
 * Class principal.
 */
public class Main {

    public static void main(final String[] args) throws SlickException {

        new PrincipalFrame().launchApplication(args);
        new AppGameContainer(new StateGame(), 800, 600, false).start();
        ConnectionHowob.getInstance().save(Player.life, Player.experience,Player.mania,  Player.potion, Player.wood, Player.steel);
        ConnectionHowob.getInstance().Logout();

    }
}
