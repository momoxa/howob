package be.masi.core;

import be.masi.httpconnection.ConnectionHowob;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public class MainScreenGameState extends BasicGameState {

    public static final int ID = 1;
    private Image background;
    private Image goblin, adventurer, golem;
    private StateBasedGame game;
    private int mouseX = 0;
    private int mouseY = 0;
    private boolean insideStartGame = false;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.background = new Image("resources/background/forest.png");
        this.goblin = new Image("resources/background/GoblinEcranAcceuil.png");
        this.adventurer = new Image("resources/background/AdventurerEcranAcceuil.png");
        this.golem = new Image("resources/background/GolemEcranAcceuil.png");
        container.setAlwaysRender(true);

    }
    /**
     * Contentons-nous d'afficher l'image de fond. .
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        background.draw(0, 0, container.getWidth(), container.getHeight());
        goblin.draw(100, 250 , 150, 150);
        adventurer.draw(300, 250, 150, 150);
        golem.draw(500, 250, 150, 150);
        g.drawString("Select your player", 300, 500);
    }

    private void getPosClicked(GameContainer gc) {
        Input input = gc.getInput();
        if (input.isMousePressed(0)) {
            mouseX = input.getMouseX();
            mouseY = input.getMouseY();
        }
    }

    /**
     * Check les entrée souris et lance le jeu quand le personnage est séléctionné
     * @param container
     * @param game
     * @param delta
     * @throws SlickException
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        getPosClicked(container);
        if(insideStartGame)
        {
            MapGameState gameState =  new MapGameState();
            gameState.init(container, game);
            game.addState(gameState);
            game.enterState(MapGameState.ID);
        }

        if ((mouseX > 100 && mouseX < goblin.getWidth() + 100) && (mouseY >= 250 && mouseY <= goblin.getHeight() + 250)) {
            MapGameState.PERSO = 1; // Goblin
            ConnectionHowob.getInstance().setCharacterId(1);
            insideStartGame = true;
        }
        else if ((mouseX > 300 && mouseX < adventurer.getWidth() + 300) && (mouseY >= 250 && mouseY <= adventurer.getHeight() + 250)) {
            MapGameState.PERSO = 2; // Aventurier
            ConnectionHowob.getInstance().setCharacterId(2);
            insideStartGame = true;
        }
        else if ((mouseX > 500 && mouseX < golem.getWidth() + 500) && (mouseY >= 250 && mouseY <= golem.getHeight() + 250)) {
            MapGameState.PERSO = 3; // Golem
            ConnectionHowob.getInstance().setCharacterId(3);
            insideStartGame = true;
        }
        else insideStartGame = false;

    }
    /**
     * L'identifiant permet d'identifier les différentes boucles, pour passer de l'une à l'autre.
     */
    @Override
    public int getID() {
        return ID;
    }
}
