package be.masi.core;

import be.masi.batiments.BatimentBoomcraftController;
import be.masi.batiments.BatimentsFarmvilleController;
import be.masi.batiments.VillageFarmVilleController;
import be.masi.camera.Camera;
import be.masi.httpconnection.ConnectionBoomCraft;
import be.masi.httpconnection.ConnectionFarmVille;
import be.masi.hud.Hud;
import be.masi.map.Map;
import be.masi.popup.Popup;
import be.masi.trigger.TriggerController;
import be.masi.bonus.BallController;
import be.masi.httpconnection.ConnectionHowob;
import be.masi.player.*;
import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
//import sun.java2d.opengl.WGLSurfaceData;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public class MapGameState extends BasicGameState{

    public static final int ID = 2;
    public static int PERSO = 0;

    private GameContainer container;
    private Map map;
    private Player player;
    private TriggerController triggers;
    private Camera camera;
    private PlayerController controller;
    private Hud hud;
    private BallController ballController;
    private BatimentBoomcraftController batimentBoomcraftController;
    private BatimentsFarmvilleController batimentsFarmvilleController;
    private VillageFarmVilleController villageFarmVilleController;
    Music background;
    Boolean diffenceList = false, first = false;
    List<Integer> batimentAAjouter = new ArrayList<Integer>();
    List<Integer> villageAAjouter = new ArrayList<Integer>();

    Popup p =  new Popup(450,550,50,50);

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        ConnectionHowob.getInstance().getCharacters();
        this.container = container;
        this.map = new Map();
        this.hud = new Hud(map);
        this.ballController = new BallController(map, hud);
        this.batimentBoomcraftController = new BatimentBoomcraftController(map, hud);
        this.villageFarmVilleController = new VillageFarmVilleController(map, hud);
        this.batimentsFarmvilleController = new BatimentsFarmvilleController(map, hud);
       // this.playerRemoteController = new PlayerRemoteController();

        // Choix du personnage
        if(PERSO == 1) {
            this.player = new Goblin(map, ballController, batimentBoomcraftController, batimentsFarmvilleController, villageFarmVilleController, hud);
            this.player.setLife(ConnectionHowob.getInstance().getGoblin()[0]);
            this.player.setExperience(ConnectionHowob.getInstance().getGoblin()[1]);
            this.player.setMania(ConnectionHowob.getInstance().getGoblin()[2]);
            this.player.setPotion(ConnectionHowob.getInstance().getGoblin()[3]);
            this.player.setWood(ConnectionHowob.getInstance().getGoblin()[4]);
            this.player.setSteel(ConnectionHowob.getInstance().getGoblin()[5]);
            hud.setPlayerBarImage("resources/hud/player-bar-goblin.png");
            ConnectionHowob.getInstance().postCharacter(1);
        }
        else if (PERSO == 2) {
            this.player = new Adventurer(map, ballController, batimentBoomcraftController, batimentsFarmvilleController, villageFarmVilleController, hud);
            this.player.setLife(ConnectionHowob.getInstance().getAdventurer()[0]);
            this.player.setExperience(ConnectionHowob.getInstance().getAdventurer()[1]);
            this.player.setMania(ConnectionHowob.getInstance().getAdventurer()[2]);
            this.player.setPotion(ConnectionHowob.getInstance().getAdventurer()[3]);
            this.player.setWood(ConnectionHowob.getInstance().getAdventurer()[4]);
            this.player.setSteel(ConnectionHowob.getInstance().getAdventurer()[5]);
            hud.setPlayerBarImage("resources/hud/player-bar-adventurer.png");
            ConnectionHowob.getInstance().postCharacter(2);
        }
        else {
            this.player = new Golem(map, ballController, batimentBoomcraftController, batimentsFarmvilleController, villageFarmVilleController, hud);
            this.player.setLife(ConnectionHowob.getInstance().getGolem()[0]);
            this.player.setExperience(ConnectionHowob.getInstance().getGolem()[1]);
            this.player.setMania(ConnectionHowob.getInstance().getGolem()[2]);
            this.player.setPotion(ConnectionHowob.getInstance().getGolem()[3]);
            this.player.setWood(ConnectionHowob.getInstance().getGolem()[4]);
            this.player.setSteel(ConnectionHowob.getInstance().getGolem()[5]);
            hud.setPlayerBarImage("resources/hud/player-bar-golem.png");
            ConnectionHowob.getInstance().postCharacter(3);
        }

        this.triggers  = new TriggerController(map, player, hud, p);
        this.camera = new Camera(player);
        this.controller = new PlayerController(player);
        //this.background = new Music("resources/sound/lost-in-the-meadows.ogg");
        //background.loop();
        this.map.init();
        this.player.init();
        this.hud.init();
        this.ballController.init();
        this.batimentBoomcraftController.init();
        this.villageFarmVilleController.init();
        this.batimentsFarmvilleController.init();
        this.controller.setInput(container.getInput());
        container.getInput().addKeyListener(controller);
        container.setForceExit(false);

        // Thread send location & receive remote players.
        new Thread(){
            public void run(){
                while (true) {
                    ConnectionHowob.getInstance().sendLocationToServer();
                    List<RemotePlayer> remotePlayers = ConnectionHowob.getInstance().getLocations();
                    hud.updateRemotePlayers(remotePlayers);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
        new Thread(){
            public void run() {
                try {
                    batimentBoomcraftController.setBatimentsBoomcrafts(ConnectionBoomCraft.getInstance().getBatiments());
                    villageFarmVilleController.setVillagesFarmVilles(ConnectionFarmVille.getInstance().getVillages());
                    first = true;
                }catch (Exception e) {
                    System.out.print("Problème premier batiemnts first ");
                    batimentBoomcraftController.getBatimentsBoomcrafts().clear();
                    villageFarmVilleController.getVillagesFarmVilles().clear();
                }

                    while (true) {
                        try {
                            batimentBoomcraftController.setBatimentsBoomcraftsTemp(ConnectionBoomCraft.getInstance().getBatiments());
                            villageFarmVilleController.setVillagesFarmVillesTemp(ConnectionFarmVille.getInstance().getVillages());
                            batimentAAjouter = batimentBoomcraftController.CompareList();
                            villageAAjouter = villageFarmVilleController.CompareList();
                            if (batimentAAjouter.size() > 0 || villageAAjouter.size() > 0) {
                                diffenceList = true;
                            }
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            System.out.print("Problème premier batiements");
                            batimentBoomcraftController.getBatimentsBoomcrafts().clear();
                            villageFarmVilleController.getVillagesFarmVilles().clear();
                        }
                    }
            }

        }.start();
        p.addString("Chargement... Veuillez patientier");
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        try {
            g.resetTransform();
            this.camera.render(container, g);
            this.map.renderBackground();
            // N'afficher les balls que dans la map solo
            if (map.getName().contains("resources/map/MainMap.tmx")) {
                this.ballController.render(g);
            }
            if (map.getName().contains("resources/map/BoomCraft.tmx")) {
                this.batimentBoomcraftController.render(g);
            }
            if (map.getName().contains("AllFarmVille")) {
                this.villageFarmVilleController.render(g);
            }
            if (map.getName().equals("resources/map/farmVille.tmx")) {
                this.batimentsFarmvilleController.render(g);
            }
            this.player.render(g);
            this.map.renderForeground();
            this.hud.render(g);
            p.render(g);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta){
        try {
            this.controller.update();
            this.triggers.update();
            this.player.update(delta);
            this.camera.update(container);
            if (map.getName().contains("resources/map/MainMap.tmx")) {
                this.ballController.update();
            }
            if (map.getName().contains("resources/map/BoomCraft.tmx")) {
                this.batimentBoomcraftController.update();
            }
            if (map.getName().contains("AllFarmVille")) {
                this.villageFarmVilleController.update();
            }
            if (map.getName().equals("resources/map/farmVille.tmx")) {
                this.batimentsFarmvilleController.update();
            }
            try {
                if (first) {
                    batimentBoomcraftController.CreateBatiments("resources/map/BoomCraft.tmx");
                    villageFarmVilleController.CreateBatiments("resources/map/AllFarmVille.tmx");
                    first = false;
                    p.deleteString();
                }

                if (diffenceList) {
                    for (int i : batimentAAjouter) {
                        batimentBoomcraftController.getBatimentsBoomcrafts().get(i).init();
                        batimentBoomcraftController.generateBatiments(i, "resources/map/BoomCraft.tmx");

                    }
                    for (int i : villageAAjouter) {
                        villageFarmVilleController.getVillagesFarmVilles().get(i).init();
                        villageFarmVilleController.generateBatiments(i, "resources/map/AllFarmVille.tmx");

                    }
                    batimentAAjouter.clear();
                    villageAAjouter.clear();
                    diffenceList = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (SlickException e)
        {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void keyReleased(int key, char c) {
        if (Input.KEY_ESCAPE == key) {
            this.container.exit();
        }
    }

    @Override
    public int getID() {
        return ID;
    }


}
