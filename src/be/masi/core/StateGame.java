package be.masi.core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public class StateGame extends StateBasedGame {

    public StateGame() {
        super("HoWoB");
    }

    /**
     * Ici il suffit d'ajouter nos deux boucles de jeux. La première ajoutée sera celle qui sera
     * utilisée au début
     */
    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        addState(new MainScreenGameState());
    }


}
