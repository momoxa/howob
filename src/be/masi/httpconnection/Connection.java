package be.masi.httpconnection;

import org.lwjgl.Sys;

/**
 * Created by Xavie on 05-12-15.
 */
public class Connection {

    private static Connection INSTANCE = null;

    public static synchronized Connection getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Connection();
        }
        return INSTANCE;
    }

    public Connection() {
    }

    public int login(String login, String password) {

        int id = 0;
        try {
            id = ConnectionHowob.getInstance().Login(login, password);
            System.out.print(id);
            if (id > 0) {
                return id;

            } else if (id == -1) {
                if (ConnectionVeggieCrush.getInstance().Login(login, password) == true) {
                    id = ConnectionHowob.getInstance().Registration(login, password, "test@test.com", "user", "user");
                    ConnectionHowob.getInstance().setLogin(login);
                    ConnectionHowob.getInstance().setPassword(password);
                    return id;
                }
                else if(ConnectionBoomCraft.getInstance().Login(login, password) == true){
                    id = ConnectionHowob.getInstance().Registration(login, password, "test@test.com", "user", "user");
                    ConnectionVeggieCrush.getInstance().registration(login, password);
                    ConnectionHowob.getInstance().setLogin(login);
                    ConnectionHowob.getInstance().setPassword(password);
                    return id;
                }
                else if(ConnectionFarmVille.getInstance().Login(login, password) == true){
                    id = ConnectionHowob.getInstance().Registration(login, password, "test@test.com", "user", "user");
                    ConnectionVeggieCrush.getInstance().registration(login, password);
                    ConnectionHowob.getInstance().setLogin(login);
                    ConnectionHowob.getInstance().setPassword(password);
                    return id;
                }
                else return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public boolean checkLogin(String login) {
        Boolean loginExist = false;
        try {
            if (ConnectionVeggieCrush.getInstance().CheckLogin(login) == true ||
                    ConnectionBoomCraft.getInstance().CheckLogin(login)  == true ||
                    ConnectionFarmVille.getInstance().CheckLogin(login) == true) {

                loginExist = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginExist;
    }
}
