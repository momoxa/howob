package be.masi.httpconnection;

import be.masi.batiments.*;
import org.json.JSONObject;
import org.lwjgl.Sys;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xavie on 29-11-15.
 */
public class ConnectionBoomCraft {

    HttpUtility httpUtility;
    private static ConnectionBoomCraft INSTANCE = null;
    private final String URL = "http://10.113.32.59:8080/BoomcraftWeb/webresources/";


    public ConnectionBoomCraft() {
        this.httpUtility = new HttpUtility();
    }
    public static synchronized ConnectionBoomCraft getInstance() {
        if (INSTANCE == null)
        { 	INSTANCE = new ConnectionBoomCraft();
        }
        return INSTANCE;
    }

    public Boolean Login (String login, String password) {
        String response = null;
        Boolean rep = null;
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "model.users/exists/" + login + "/" + password);
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  rep;
    }
    public Boolean CheckLogin (String login) {
        String response = null;
        Boolean rep = null;
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "model.users/exists/" + login);
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  rep;
    }
    public List<Batiments> getBatiments() throws  Exception{
        List<JSONObject> response = null;
        List<Batiments> batimentsBoomcrafts = new ArrayList<Batiments>();

            URLConnection httpsURLConnection = httpUtility.sendGetRequest(URL + "model.element/buildingHowob");
            response = httpUtility.readObjectsJson(httpsURLConnection);


        if (response != null && response.size() > 0) {
            for (JSONObject j : response) {
                if (Integer.valueOf(j.get("idFaction").toString()) == ConnectionHowob.getInstance().getTeamId()) {
                    if (Integer.valueOf(j.get("currentlife").toString()) < Integer.valueOf(j.get("maxlife").toString())) {
                        batimentsBoomcrafts.add(new BatimentsBoomCraftAllie(
                                (Integer.valueOf(j.get("id").toString())),
                                (String.valueOf(j.get("name").toString())),
                                (Integer.valueOf(j.get("idFaction").toString())),
                                (Integer.valueOf(j.get("defense").toString())),
                                (Integer.valueOf(j.get("maxlife").toString())),
                                (Integer.valueOf(j.get("currentlife").toString()))
                        ));
                    }
                } else {
                    if (Integer.valueOf(j.get("currentlife").toString()) > 0) {
                        batimentsBoomcrafts.add(new BatimentsBoomCraftEnnemi(
                                (Integer.valueOf(j.get("id").toString())),
                                (String.valueOf(j.get("name").toString())),
                                (Integer.valueOf(j.get("idFaction").toString())),
                                (Integer.valueOf(j.get("defense").toString())),
                                (Integer.valueOf(j.get("maxlife").toString())),
                                (Integer.valueOf(j.get("currentlife").toString()))
                        ));
                    }
                }

            }
        }
            return batimentsBoomcrafts;
        }
    public boolean sendAction(int idBatiments, float life) {

        String response = null;
        Boolean rep = false;
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "model.element/newBuildingLife/" + idBatiments + "/" + String.valueOf((int)life));
            System.out.print("Life : " + life);
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
            System.out.print("rep  : " + rep );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return rep;
    }
}
