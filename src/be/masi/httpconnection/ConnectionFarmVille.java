package be.masi.httpconnection;

import be.masi.batiments.*;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xavie on 29-11-15.
 */
public class ConnectionFarmVille {

    HttpUtility httpUtility;
    private static ConnectionFarmVille INSTANCE = null;
    private final String URL = "http://bulpa.familyds.org/FarmVillage/Webservice/index.php?method=";

    public ConnectionFarmVille() {
        this.httpUtility = new HttpUtility();
    }
    public static synchronized ConnectionFarmVille getInstance() {
        if (INSTANCE == null)
        { 	INSTANCE = new ConnectionFarmVille();
        }
        return INSTANCE;
    }
    public Boolean Login (String login, String password) {
        String response = null;
        Boolean rep = null;

        Map<String, String> params = new HashMap<String, String>();
        params.put("login", String.valueOf(login));
        params.put("password", String.valueOf(password));
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "checkUserAll" + "&login=" + login + "&password=" + password );
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  rep;
    }
    public Boolean CheckLogin (String login) {
        String response = null;
        Boolean rep = null;

        Map<String, String> params = new HashMap<String, String>();

        params.put("login", String.valueOf(login));
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "checkLogin" + "&login=" + login);
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  rep;
    }

    public List<Batiments> getVillages() {
        List<JSONObject> response = null;
        List<Batiments> villages = new ArrayList<Batiments>();
        try {
            URLConnection httpsURLConnection = httpUtility.sendGetRequest(URL + "users");
            response = httpUtility.readObjectsJson(httpsURLConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (response != null && response.size() > 0) {
            for (JSONObject j : response) {
                int teamId = 0;
                if(j.get("faction").toString().contains("Bleu")) {
                    teamId = 1;
                }
                else if(j.get("faction").toString().contains("Rouge")){
                    teamId = 2;
                }
                if (teamId == ConnectionHowob.getInstance().getTeamId()) {
                    if (Integer.valueOf(j.get("coordonnee").toString()) < Integer.valueOf(j.get("vieMax").toString())) {
                        villages.add(new VillageFarmVilleAllie(
                                (String.valueOf(j.get("pseudo").toString())),
                                teamId,
                                (Integer.valueOf(j.get("vieMax").toString())),
                                (Integer.valueOf(j.get("coordonnee").toString()))
                        ));
                    }
                } else {
                    if (Integer.valueOf(j.get("coordonnee").toString()) > 0) {
                        villages.add(new VillageFarmVilleEnnemi(
                                (String.valueOf(j.get("pseudo").toString())),
                                teamId,
                                (Integer.valueOf(j.get("vieMax").toString())),
                                (Integer.valueOf(j.get("coordonnee").toString()))
                        ));
                    }
                }

            }
        }
        return villages;
    }

    public List<Batiments> getBatiments(String login) {
        List<JSONObject> response = null;
        List<Batiments> batiments = new ArrayList<Batiments>();
        try {
            URLConnection httpsURLConnection = httpUtility.sendGetRequest(URL + "batimentdefense&login=" + login);
            response = httpUtility.readObjectsJson(httpsURLConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (response != null && response.size() > 0) {
            for (JSONObject j : response) {
                batiments.add(new BatimentsFarmville((Integer.valueOf(j.get("defenseN").toString()))));
            }
        }
        return batiments;
    }

    public boolean sendAction(int idBatiments, float life) {

        String response = null;
        Boolean rep = false;
        try {
            URLConnection URLConnection = httpUtility.sendGetRequest(URL + "model.element/newBuildingLife/" + idBatiments + "/" + String.valueOf((int)life));
            System.out.print("Life : " + life);
            response = httpUtility.readSingleLineResponse(URLConnection);
            rep = Boolean.valueOf(response);
            System.out.print("rep  : " + rep );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return rep;
    }




}
