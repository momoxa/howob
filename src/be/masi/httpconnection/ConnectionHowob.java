package be.masi.httpconnection;

import be.masi.player.RemotePlayer;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.*;


public class ConnectionHowob {

    private int[] adventurer = new int[6];
    private int[] goblin = new int[6];
    private int[] golem = new int[6];
    private int playerId, characterId, teamId;
    private String sessionId;
    private List<RemotePlayer> remotePlayers;
    private String map = "MainMap", batiment = "-1";
    private int x,y;
    private HttpUtility httpUtility;
    private String login, password;
    String url = "http://10.113.32.52/howob/public/";

    private static ConnectionHowob INSTANCE = null;
    public ConnectionHowob() {
        httpUtility = new HttpUtility();
        remotePlayers = new ArrayList<RemotePlayer>();
    }
    public static synchronized ConnectionHowob getInstance() {
        if (INSTANCE == null)
        { 	INSTANCE = new ConnectionHowob();
        }
        return INSTANCE;
    }
    public int Login(String login, String password){

        Map<String, String> params = new HashMap<String, String>();
        String requestURL = url + "login";
        params.put("login", login);
        params.put("password", password);
        try {
            HttpURLConnection httpURLConnection = httpUtility.sendPostRequest(requestURL, params);
            JSONObject response = httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            playerId = Integer.valueOf(response.get("id").toString());
            if(playerId > 0)
            sessionId = response.get("session_id").toString();
            return playerId;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return -1;
    }
    public String Logout(){

        Map<String, String> params = new HashMap<String, String>();
        String requestURL = url + "logout";
        params.put("id_session", sessionId);
        params.put("id", String.valueOf(playerId));
        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendPostRequest(requestURL, params);
            JSONObject response =  httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            return response.get("response").toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "ko";
    }
    public int Registration(String login, String password, String email, String firstname, String lastname){

        Map<String, String> params = new HashMap<String, String>();
        String requestURL = url + "registration";

        params.put("login", login);
        params.put("password", password);
        params.put("email", email);
        params.put("firstname", firstname);
        params.put("lastname", lastname);

        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendPostRequest(requestURL, params);
            JSONObject response =  httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            playerId =  Integer.valueOf(response.get("id").toString());
            if(playerId > 0) sessionId = response.get("session_id").toString();
            return playerId;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return -1;
    }
    public void getCharacters() {
        List<JSONObject> response = null;
        String requestURL = url + "characters/" + playerId;
        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendGetRequest(requestURL);
            response = httpUtility.readObjectsJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
        } catch (IOException e) {
            e.printStackTrace();
        }

        adventurer[0] = Integer.valueOf(response.get(0).get("life").toString());
        goblin[0] = Integer.valueOf(response.get(1).get("life").toString());
        golem[0] = Integer.valueOf(response.get(2).get("life").toString());

        adventurer[1] = Integer.valueOf(response.get(0).get("experience").toString());
        goblin[1] = Integer.valueOf(response.get(1).get("experience").toString());
        golem[1] = Integer.valueOf(response.get(2).get("experience").toString());

        adventurer[2] = Integer.valueOf(response.get(0).get("mania").toString());
        goblin[2] = Integer.valueOf(response.get(1).get("mania").toString());
        golem[2] = Integer.valueOf(response.get(2).get("mania").toString());

        adventurer[3] = Integer.valueOf(response.get(0).get("potion").toString());
        goblin[3] = Integer.valueOf(response.get(1).get("potion").toString());
        golem[3] = Integer.valueOf(response.get(2).get("potion").toString());

        adventurer[4] = Integer.valueOf(response.get(0).get("wood").toString());
        goblin[4] = Integer.valueOf(response.get(1).get("wood").toString());
        golem[4] = Integer.valueOf(response.get(2).get("wood").toString());

        adventurer[5] = Integer.valueOf(response.get(0).get("steel").toString());
        goblin[5] = Integer.valueOf(response.get(1).get("steel").toString());
        golem[5] = Integer.valueOf(response.get(2).get("steel").toString());
    }
    public String save(int life, int experience, int mania, int potion, int wood, int steel) {
        String requestURL = url + "save/";
        if(characterId == 1) {
             requestURL += "goblin";
        } else if(characterId == 2){
            requestURL += "adventurer" ;
        } else {
            requestURL += "golem" ;
        }

        Map<String, String> params = new HashMap<String, String>();

        params.put("id", String.valueOf(playerId));
        params.put("session_id", String.valueOf(sessionId));
        params.put("life", String.valueOf(life));
        params.put("experience", String.valueOf(experience));
        params.put("mania", String.valueOf(mania));
        params.put("potion", String.valueOf(potion));
        params.put("wood", String.valueOf(wood));
        params.put("steel", String.valueOf(steel));

        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendPostRequest(requestURL, params);
            JSONObject response =  httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            return response.get("response").toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "ko";
    }
    public String postCharacter(int characterId) {
        String requestURL = url + "character";
        String character ="";
        Map<String, String> params = new HashMap<String, String>();

        if(characterId == 1) {
            character = "goblin";
        } else if(characterId == 2){
            character = "adventurer" ;
        } else {
            character = "golem" ;
        }

        params.put("id", String.valueOf(playerId));
        params.put("session_id", String.valueOf(sessionId));
        params.put("character", String.valueOf(character));
        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendPostRequest(requestURL, params);
            JSONObject response =  httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            return response.get("response").toString();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "ko";

    }
    public String sendLocationToServer(){
        String requestURL = url + "update/location";

        Map<String, String> params = new HashMap<String, String>();

        params.put("id", String.valueOf(playerId));
        params.put("session_id", String.valueOf(sessionId));
        params.put("map", String.valueOf(map));
        params.put("building", String.valueOf(batiment));
        params.put("player_x", String.valueOf(x));
        params.put("player_y", String.valueOf(y));
        try {
            HttpURLConnection httpURLConnection =  httpUtility.sendPostRequest(requestURL, params);
            JSONObject response =  httpUtility.readObjectJson(httpURLConnection);
            httpUtility.disconnect(httpURLConnection);
            return response.get("response").toString();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "ko";
    }
     public List<RemotePlayer> getLocations() {
         remotePlayers.clear();
         String requestURL = url + "locations";
         List<JSONObject> response = null;

         try {
             HttpURLConnection httpURLConnection =  httpUtility.sendGetRequest(requestURL);
             response = httpUtility.readObjectsJson(httpURLConnection);
             httpUtility.disconnect(httpURLConnection);
         } catch (IOException e) {
             e.printStackTrace();
         }

         if(response != null && response.size() > 0) {
             for (JSONObject o : response) {
                 if (Integer.valueOf(o.get("id").toString()) != playerId)
                     remotePlayers.add(new RemotePlayer((Float.valueOf(o.get("x").toString())), (Float.valueOf(o.get("y").toString())),
                             o.get("building").toString(), o.get("map").toString(), o.get("login").toString(), Integer.valueOf(o.get("id").toString()), o.get("character").toString()));
             }
         }
         return remotePlayers;
     }
    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }
    public int[] getAdventurer() {
        return adventurer;
    }
    public int[] getGoblin() {
        return goblin;
    }
    public int[] getGolem() {
        return golem;
    }
    public void sendLocation(String map, String batiment) {
        this.map = map;
        this.batiment = batiment;

    }
    public void sendLocation(int playerX, int playerY){
        this.x = playerX;
        this.y = playerY;

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTeamId() {
        return teamId;
    }
}
