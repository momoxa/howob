package be.masi.httpconnection;

import be.masi.player.Player;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xavie on 29-11-15.
 */
public class ConnectionVeggieCrush {

    HttpsUtility httpsUtility;
    private static ConnectionVeggieCrush INSTANCE = null;
    private final String URL = "https://10.113.32.55:8734";

    public ConnectionVeggieCrush() {
        this.httpsUtility = new HttpsUtility();
    }
    public static synchronized ConnectionVeggieCrush getInstance() {
        if (INSTANCE == null)
        { 	INSTANCE = new ConnectionVeggieCrush();
        }
        return INSTANCE;
    }
    public Boolean registration(String login, String password){

        String response = null;
        JSONObject Faction   = new JSONObject();
        JSONObject parent = new JSONObject();

        Faction.put("Id", "1");
        Faction.put("Name", "noFaction");
        parent.put("Faction", Faction);
        parent.put("Id", "0");
        parent.put("Name", login);
        parent.put("Password", password);
        parent.put("Subscription", "free");

        try {
            URLConnection httpsURLConnection=httpsUtility.sendRequestJson(URL + "/users/", parent, true);
            response = httpsUtility.readSingleLineResponse(httpsURLConnection);
            Boolean rep = Boolean.valueOf(response);
            if(rep){
                return true;
            }
            else return false;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }
    /**
     * Ne pas appeller directement. Méthode appellée dans Connection. Crée une entrée dans notre db pour ajouter le user.
     * @param login
     * @param password
     * @return
     */
    public Boolean Login (String login, String password) {

        String response = null;
        JSONObject Faction   = new JSONObject();
        JSONObject parent = new JSONObject();

        Faction.put("Id", "1");
        Faction.put("Name", "noFaction");
        parent.put("Faction", Faction);
        parent.put("Id", "0");
        parent.put("Name", login);
        parent.put("Password", password);
        parent.put("Subscription", "free");

        try {
            URLConnection httpsURLConnection=httpsUtility.sendRequestJson(URL + "/users/login", parent, true);
            response = httpsUtility.readSingleLineResponse(httpsURLConnection);
            Boolean rep = Boolean.valueOf(response);
            if(rep){
                return true;
            }
            else return false;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }
    public Boolean CheckLogin (String login) {
        JSONObject response = null;
        try {
            URLConnection httpsURLConnection = httpsUtility.sendGetRequest(URL + "/users/name=" + login);
            response = httpsUtility.readObjectJson(httpsURLConnection);
           // httpsUtility.disconnect(httpsURLConnection);
            if(response != null)
            {
                return true;
            }
            else return false;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getLifeAvailable () {
        JSONObject response = null;
        int lifeDisponible = 0;
        try {
            URLConnection httpsURLConnection = httpsUtility.sendGetRequest(URL + "/users/" + ConnectionHowob.getInstance().getLogin() + "/item");
            response = httpsUtility.readObjectJson(httpsURLConnection);
            if(response!= null) {
                lifeDisponible = Integer.valueOf(response.get("Life").toString());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return lifeDisponible;
    }

    public int getRessources(){
        int lifeDispo = getLifeAvailable();
        if(lifeDispo >= 20){
            if(deleteRessources(lifeDispo - 20)){
                return 20;
            }
        }
        return 0;
    }
    public boolean deleteRessources(int quantity) {

        String response = null;
        JSONObject parent = new JSONObject();

        parent.put("Id", "1");
        parent.put("Life", 20);

        try {
            URLConnection httpsURLConnection = httpsUtility.sendRequestJson(URL + "/users/" + ConnectionHowob.getInstance().getLogin() + "/item/" + "removeLife" , parent, true);
            response = httpsUtility.readSingleLineResponse(httpsURLConnection);
            Boolean rep = Boolean.valueOf(response);
            if(rep){
                return true;
            }
            else return false;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }

    public boolean sendRessources() {

        String response = null;
        JSONObject Faction   = new JSONObject();
        JSONObject parent = new JSONObject();

        parent.put("Id", "4");
        parent.put("Name", "Potion");
        parent.put("Quantity", 1);

        try {
            URLConnection httpsURLConnection=httpsUtility.sendRequestJson(URL + "/users/" + ConnectionHowob.getInstance().getLogin() + "/resources/" + "potion" , parent, true);
            response = httpsUtility.readSingleLineResponse(httpsURLConnection);
            Boolean rep = Boolean.valueOf(response);
            if(rep){
                return true;
            }
            else return false;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }




}
