package be.masi.httpconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

    /**
     * Created by Xavie on 05-11-15.
     * This class encapsulates methods for requesting a server via HTTP GET/POST and
     * provides methods for parsing response from the server.
     */
    public class HttpUtility {

        /**
         * Makes an HTTP request using GET method to the specified URL.
         *
         * @param requestURL
         *            the URL of the remote server
         * @return An HttpURLConnection object
         * @throws IOException
         *             thrown if any I/O error occurred
         */
        public synchronized HttpURLConnection sendGetRequest(String requestURL) throws IOException {
            HttpURLConnection httpConn;
            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoInput(true); // true if we want to read server's response
            httpConn.setDoOutput(false); // false indicates this is a GET request
            return httpConn;
        }

        /**
         * Makes an HTTP request using POST method to the specified URL.
         *
         * @param requestURL
         *            the URL of the remote server
         * @param params
         *            A map containing POST data in form of key-value pairs
         * @return An HttpURLConnection object
         * @throws IOException
         *             thrown if any I/O error occurred
         */
        public synchronized HttpURLConnection sendPostRequest(String requestURL, Map<String, String> params) throws IOException {
            HttpURLConnection httpConn;
            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);

            httpConn.setDoInput(true); // true indicates the server returns response

            StringBuffer requestParams = new StringBuffer();

            if (params != null && params.size() > 0) {

                httpConn.setDoOutput(true); // true indicates POST request

                // creates the params string, encode them using URLEncoder
                Iterator<String> paramIterator = params.keySet().iterator();
                while (paramIterator.hasNext()) {
                    String key = paramIterator.next();
                    String value = params.get(key);
                    requestParams.append(URLEncoder.encode(key, "UTF-8"));
                    requestParams.append("=").append(
                            URLEncoder.encode(value, "UTF-8"));
                    requestParams.append("&");
                }

                // sends POST data
                OutputStreamWriter writer = new OutputStreamWriter(
                        httpConn.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();
            }

            return httpConn;
        }

        /**
         * Returns only one line from the server's response. This method should be
         * used if the server returns only a single line of String.
         *
         * @return a String of the server's response
         * @throws IOException
         *             thrown if any I/O error occurred
         */
        public synchronized String readSingleLineResponse(URLConnection httpConn) throws IOException {
            InputStream inputStream = null;
            if (httpConn != null) {
                inputStream = httpConn.getInputStream();
            } else {
                throw new IOException("Connection is not established.");
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream));

            String response = reader.readLine();
            reader.close();

            return response;
        }

        public synchronized JSONObject readObjectJson(URLConnection httpConn) throws IOException {
            InputStream inputStream = null;
            if (httpConn != null) {
                inputStream = httpConn.getInputStream();
            } else {
                throw new IOException("Connection is not established.");
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String response = reader.readLine();
            reader.close();

            JSONObject jsonObject = new JSONObject(response);

            return jsonObject;
        }

        /**
         * Returns an array of lines from the server's response. This method should
         * be used if the server returns multiple lines of String.
         *
         * @return an array of Strings of the server's response
         * @throws IOException
         *             thrown if any I/O error occurred
         */
        public synchronized String[] readMultipleLinesResponse(HttpURLConnection httpConn) throws IOException {
            InputStream inputStream = null;
            if (httpConn != null) {
                inputStream = httpConn.getInputStream();
            } else {
                throw new IOException("Connection is not established.");
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream));
            List<String> response = new ArrayList<String>();
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.add(line);
            }
            reader.close();

            return (String[]) response.toArray(new String[0]);
        }

        public synchronized List<JSONObject> readObjectsJson(URLConnection httpConn) throws IOException {
            InputStream inputStream = null;
            if (httpConn != null) {
                inputStream = httpConn.getInputStream();
            } else {
                throw new IOException("Connection is not established.");
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream));
            StringBuilder response = new StringBuilder();
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            JSONArray jsonObj = new JSONArray(response.toString());
            List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
            for(int i = 0; i<jsonObj.length(); i++) {
                jsonObjects.add(jsonObj.getJSONObject(i));
            }
            return jsonObjects;
        }

        /**
         * Closes the connection if opened
         */
        public synchronized void disconnect(HttpURLConnection httpConn) {
            if (httpConn != null) {
                httpConn.disconnect();
            }
        }


    }

