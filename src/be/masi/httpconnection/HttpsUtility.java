package be.masi.httpconnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.lwjgl.Sys;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Created by Xavie on 23-11-15.
 */
public class HttpsUtility {

    static HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {return true;
        }
    };
    TrustManager[] trustAllCerts = IgnoreCertificats();
    String userpass = "zavier" + ":" + "password";
    String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));

    private void initHttpsSocket(){
        try {
            // Ignorer le certificats
            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create SSl socket
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }
    private TrustManager[] IgnoreCertificats() {
        // Ignorer le certificat
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        return trustAllCerts;
    }
    /**
     * Liste tous les utilisateurs existant : a appeler lors du login
     * @throws Exception
     */
    public synchronized URLConnection sendGetRequest(String requestURL) throws Exception {
        URLConnection httpsConn = null;
        try {
            initHttpsSocket();
            URL url = new URL(requestURL);
            httpsConn = url.openConnection();
            httpsConn.setRequestProperty("Authorization", basicAuth);
            httpsConn.setUseCaches(false);
            httpsConn.setDoInput(true); // true if we want to read server's response

        } catch (Exception e) {
            e.printStackTrace();
        }
        return httpsConn;
    }
    public synchronized HttpsURLConnection sendRequest(String requestURL, Map<String, String> params, Boolean POST) throws IOException {
       HttpsURLConnection httpsConn = null;
        try {

            initHttpsSocket();
            URL url = new URL(requestURL);
            httpsConn = (HttpsURLConnection) url.openConnection();
            httpsConn.setRequestProperty("Authorization", basicAuth);
            httpsConn.setUseCaches(false);
            httpsConn.setDoInput(true); // true indicates the server returns response
            StringBuffer requestParams = new StringBuffer();
            if (params != null && params.size() > 0) {

                httpsConn.setDoOutput(true); // true indicates POST request
                if(POST){
                httpsConn.setRequestMethod("POST");}
                else {httpsConn.setRequestMethod("PUT");}


                // creates the params string, encode them using URLEncoder
                Iterator<String> paramIterator = params.keySet().iterator();
                while (paramIterator.hasNext()) {
                    String key = paramIterator.next();
                    String value = params.get(key);
                    requestParams.append(URLEncoder.encode(key, "UTF-8"));
                    requestParams.append("=").append(
                            URLEncoder.encode(value, "UTF-8"));
                    requestParams.append("&");
                }
                // sends POST data
                OutputStreamWriter writer = new OutputStreamWriter(
                        httpsConn.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return httpsConn;
    }
    public synchronized URLConnection sendRequestJson(String requestURL, JSONObject json, Boolean POST) throws IOException {
        URLConnection httpsConn = null;
        try {
            initHttpsSocket();
            URL url = new URL(requestURL);
            httpsConn = (URLConnection) url.openConnection();
            httpsConn.setRequestProperty("Authorization", basicAuth);
            httpsConn.setUseCaches(false);
            httpsConn.setDoInput(true); // true indicates the server returns response
            httpsConn.setRequestProperty("Content-Type", "application/json");
            httpsConn.setRequestProperty("Accept", "application/json");

            if (json != null) {
                httpsConn.setDoOutput(true); // true indicates POST request
                OutputStreamWriter writer = new OutputStreamWriter(httpsConn.getOutputStream());
                writer.write(json.toString());
                writer.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return httpsConn;
    }
    public synchronized URLConnection sendDeleteJson(String requestURL, JSONObject json, Boolean POST) throws IOException {
        URLConnection httpsConn = null;
        try {
            initHttpsSocket();
            URL url = new URL(requestURL);
            httpsConn = (URLConnection) url.openConnection();
            httpsConn.setRequestProperty("Authorization", basicAuth);
            httpsConn.setUseCaches(false);
            httpsConn.setDoInput(true); // true indicates the server returns response
            httpsConn.setRequestProperty("Content-Type", "application/json");
            httpsConn.setRequestProperty("Accept", "application/json");

            if(json != null) {
                httpsConn.setDoOutput(true); // true indicates POST request
                OutputStreamWriter writer = new OutputStreamWriter(httpsConn.getOutputStream());
                writer.write(json.toString());
                writer.flush();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return httpsConn;
    }
    public synchronized void disconnect(URLConnection httpsConn) throws IOException {
        if (httpsConn != null) {
            httpsConn.getOutputStream().close();
        }
    }

    public synchronized JSONObject readObjectJson(URLConnection httpConn) throws IOException {
        InputStream inputStream = null;
        JSONObject jsonObject = null;
        if (httpConn != null) {
                inputStream = httpConn.getInputStream();
        } else {
            throw new IOException("Connection is not established.");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String response = reader.readLine();
        reader.close();

        if(response != null) {
            jsonObject = new JSONObject(response);
        }

        return jsonObject;
    }
    public  List<JSONObject> readObjectsJson(URLConnection httpsConn) throws IOException {
        InputStream inputStream = null;
        if (httpsConn != null) {
            inputStream = httpsConn.getInputStream();
        } else {
            throw new IOException("Connection is not established.");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder response = new StringBuilder();
        String line = "";
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        reader.close();

        JSONArray jsonObj = new JSONArray(response.toString());
        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        for(int i = 0; i<jsonObj.length(); i++) {
            jsonObjects.add(jsonObj.getJSONObject(i));
        }
        return jsonObjects;
    }

    public  String readSingleLineResponse(URLConnection httpConn) throws IOException {
        InputStream inputStream = null;
        if (httpConn != null) {
            inputStream = httpConn.getInputStream();
        } else {
            throw new IOException("Connection is not established.");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));

        String response = reader.readLine();
        reader.close();

        return response;
    }

}


