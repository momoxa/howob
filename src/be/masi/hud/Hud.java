package be.masi.hud;

import be.masi.batiments.Batiments;
import be.masi.bonus.Ball;
import be.masi.map.Map;
import be.masi.player.RemotePlayer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.List;

/**
 * @author Mohamed Amine Hosni, Xavier Conrath
 * @version 1.0
 * Class permettant d'afficher le niveau de vie, expérience du joueur à l'écran.
 */
public class Hud {


    // Détermine la position de l'image à l'écran (10,10)
    private  final int P_BAR_X = 10;
    private  final int P_BAR_Y = 10;
    private  final int P_BAR_POTION_X = 500;
    private  final int P_BAR_POTION_Y = 10;
    private  final int P_BAR_WOOD_X = 600;
    private  final int P_BAR_WOOD_Y = 10;
    private  final int P_BAR_STEEL_X = 700;
    private  final int P_BAR_STEEL_Y = 10;
    private  final int P_BAR_MINI_MAP_X = 10;
    private  final int P_BAR_MINI_MAP_Y = 480;

    // Determine le X des 3 rectangle de couleur dessiner à l'écran (84 étant la taille de l'image non remplissable)
    private  final int BAR_X = 84 + P_BAR_X;
    private  final int LIFE_BAR_Y = 4 + P_BAR_Y;
    private  final int MANA_BAR_Y = 24 + P_BAR_Y;
    private  final int XP_BAR_Y = 44 + P_BAR_Y;
    private  final int BAR_WIDTH = 80;
    private  final int BAR_HEIGHT = 16;

    private  final Color LIFE_COLOR = new Color(255, 0, 0); // Rouge
    private  final Color MANA_COLOR = new Color(0, 0, 255); // Bleu
    private  final Color XP_COLOR = new Color(0, 255, 0); // Vert


    // Ratio de remplissage des bar à modifier en fonction de la vie du perso
    public  float LIFE_BAR_RATIO = 0f;
    public  float MANA_BAR_RATIO = 0f;
    public  float XP_BAR_RATIO = 0f;
    public  int NB_POTION = 0;
    public  int NB_WOOD = 0;
    public  int NB_STEEL = 0;

    private Image playerbars;
    private Image backPack;
    private Image potion;
    private Image wood;
    private Image steel;
    private Image miniMap;

    private float xPlayer = 300;
    private float yPlayer = 300;
    private List<Ball> bonus;
    private List<Batiments> batiments;
    private List<RemotePlayer> remotePlayers;

    private Map map;

    private String playerBarImage = "resources/hud/player-bar.png";

    public Hud(Map map) {
        this.map = map;
    }


    public void init() throws SlickException {
        this.playerbars = new Image(playerBarImage);
        this.potion = new Image("resources/hud/potion.png");
        this.wood = new Image("resources/hud/wood.png");
        this.steel = new Image("resources/hud/Steel.png");
        this.miniMap = new Image("resources/miniMap/MainMap.png");
    }

    public synchronized void render(Graphics g) {
        g.resetTransform();
        g.setColor(LIFE_COLOR);
        g.fillRect(BAR_X, LIFE_BAR_Y, LIFE_BAR_RATIO * BAR_WIDTH, BAR_HEIGHT);
        g.setColor(MANA_COLOR);
        g.fillRect(BAR_X, MANA_BAR_Y, MANA_BAR_RATIO * BAR_WIDTH, BAR_HEIGHT);
        g.setColor(XP_COLOR);
        g.fillRect(BAR_X, XP_BAR_Y, XP_BAR_RATIO * BAR_WIDTH, BAR_HEIGHT);
        g.drawImage(playerbars, P_BAR_X, P_BAR_Y);

        g.drawImage(potion, P_BAR_POTION_X, P_BAR_POTION_Y);
        g.setColor(Color.red);
        g.drawString(String.valueOf(NB_POTION), P_BAR_POTION_X + potion.getWidth() + 3, P_BAR_POTION_Y);

        g.drawImage(wood, P_BAR_WOOD_X, P_BAR_WOOD_Y);
        g.setColor(Color.red);
        g.drawString(String.valueOf(NB_WOOD), P_BAR_WOOD_X + wood.getWidth() + 3, P_BAR_WOOD_Y);

        g.drawImage(steel, P_BAR_STEEL_X, P_BAR_STEEL_Y);
        g.setColor(Color.red);
        g.drawString(String.valueOf(NB_STEEL), P_BAR_STEEL_X + steel.getWidth() + 3, P_BAR_STEEL_Y);

        if (!map.getName().contains("Interior") && !map.getName().contains("VeggieCrush")) {
            g.drawImage(miniMap, P_BAR_MINI_MAP_X, P_BAR_MINI_MAP_Y);
            g.setColor(Color.red);

            g.fillOval(P_BAR_MINI_MAP_X + (xPlayer / (map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth())) * (miniMap.getWidth() - 5),
                    P_BAR_MINI_MAP_Y + (yPlayer / (map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight())) * (miniMap.getHeight() - 5), 5, 5);

            g.setColor(Color.blue);

            if (map.getName().contains("MainMap")) {
                for (Ball b : bonus) {

                    g.fillOval((P_BAR_MINI_MAP_X + (b.getX() / (map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth())) * (miniMap.getWidth())) -3,
                            (P_BAR_MINI_MAP_Y + (b.getY() / (map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight())) * (miniMap.getHeight())) -3, 3, 3);
                }
            }
            g.setColor(Color.green);
            if (map.getName().contains("BoomCraft") || map.getName().contains("AllFarmVille") || map.getName().contains("farmVille")) {
              if(batiments != null && batiments.size() > 0) {
                  for (Batiments b : batiments) {
                      g.fillOval(P_BAR_MINI_MAP_X + (((b.getX() + b.getAnimations()[0].getWidth()/2)) / (map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth())) * (miniMap.getWidth()),
                              P_BAR_MINI_MAP_Y + (((b.getY() + b.getAnimations()[0].getHeight()/2)) / (map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight())) * (miniMap.getHeight()), 3, 3);
                  }
              }
            }
            g.setColor(Color.orange);
            if (remotePlayers != null) {
                for (RemotePlayer l : remotePlayers) {
                    if (map.getName().contains(l.getCarte())) {
                        g.fillOval(P_BAR_MINI_MAP_X + (l.getX() / (map.getTiledMap().getWidth() * map.getTiledMap().getTileWidth())) * (miniMap.getWidth()) -3,
                                P_BAR_MINI_MAP_Y + (l.getY() / (map.getTiledMap().getHeight() * map.getTiledMap().getTileHeight())) * (miniMap.getHeight()) -3, 3, 3);
                    }
                }
            }
        }
    }

    public void setPlayerBarImage(String playerBarImage) {
        this.playerBarImage = playerBarImage;
    }

    public void changeImage(String newmap) {
        try {
            miniMap = new Image("resources/miniMap/" + newmap);
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public void updatePlayer(int x, int y){
        this.xPlayer = x;
        this.yPlayer = y;
    }
    public void updateBonus(List<Ball> bonus)
    {
        this.bonus = bonus;
    }
    public void updateBatiments(List<Batiments> batiments)
    {
        this.batiments = batiments;
    }
    public void updateRemotePlayers(List<RemotePlayer> players)
    {
        this.remotePlayers = players;
    }

    public void setLIFE_BAR_RATIO(float LIFE_BAR_RATIO) {
        this.LIFE_BAR_RATIO = LIFE_BAR_RATIO;
    }

    public void setMANA_BAR_RATIO(float MANA_BAR_RATIO) {
        this.MANA_BAR_RATIO = MANA_BAR_RATIO;
    }

    public void setXP_BAR_RATIO(float XP_BAR_RATIO) {
        this.XP_BAR_RATIO = XP_BAR_RATIO;
    }

    public void setNB_POTION(int NB_POTION) {
        this.NB_POTION = NB_POTION;
    }

    public void setNB_WOOD(int NB_WOOD) {
        this.NB_WOOD = NB_WOOD;
    }

    public void setNB_STEEL(int NB_STEEL) {
        this.NB_STEEL = NB_STEEL;
    }

}
