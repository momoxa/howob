package be.masi.player;

import be.masi.batiments.BatimentBoomcraftController;
import be.masi.batiments.BatimentsFarmvilleController;
import be.masi.batiments.VillageFarmVilleController;
import be.masi.hud.Hud;
import be.masi.map.Map;
import be.masi.bonus.BallController;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * @author Mohamed Amine Hosni, Xavier Conrath
 * @version 1.0
 * Class représentant le personnage Goblin (médecin) héritant des propriété des joueurs.
 */
public class Goblin extends Player {

    public Goblin(Map map,
                  BallController ballController,
                  BatimentBoomcraftController batimentBoomcraftController,
                  BatimentsFarmvilleController batimentsFarmvilleController,
                  VillageFarmVilleController villageFarmVilleController,
                  Hud hud) {


        super(map, 8, 16, 8, ballController, batimentBoomcraftController, batimentsFarmvilleController, villageFarmVilleController, hud);
    }

    public void init() throws SlickException {
        SpriteSheet spriteSheet = new SpriteSheet("resources/sprites/people/goblin.png", 64, 64);

        super.animations[0] = loadAnimation(spriteSheet, 0, 1, 2);
        super.animations[1] = loadAnimation(spriteSheet, 0, 1, 3);
        super.animations[2] = loadAnimation(spriteSheet, 0, 1, 0);
        super.animations[3] = loadAnimation(spriteSheet, 0, 1, 1);
        super.animations[4] = loadAnimation(spriteSheet, 1, 8, 2);
        super.animations[5] = loadAnimation(spriteSheet, 1, 8, 3);
        super.animations[6] = loadAnimation(spriteSheet, 1, 8, 0);
        super.animations[7] = loadAnimation(spriteSheet, 1, 8, 1);
    }
}
