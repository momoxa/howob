package be.masi.player;

import be.masi.batiments.*;
import be.masi.httpconnection.Connection;
import be.masi.httpconnection.ConnectionFarmVille;
import be.masi.httpconnection.ConnectionHowob;
import be.masi.hud.Hud;
import be.masi.map.Map;
import be.masi.bonus.BallController;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public abstract class Player {

    static float x = 300, y = 300;
    protected boolean onStair = false;
    protected Animation[] animations;
    protected float dx = 0, dy = 0;
    protected int direction, fillOvalX, fillOvalY;
    protected int keyx = 0;
    public static int experience = 0;
    public static int mania = 0;
    public static int life = 100;
    public static int potion = 0;
    public static int wood = 0;
    public static int steel = 0;
    public Batiments batimentIn = null;


    private BallController ballController;
    private BatimentBoomcraftController batimentBoomcraftController;
    private BatimentsFarmvilleController batimentsFarmvilleController;
    private VillageFarmVilleController villageFarmVilleController;
    private Hud hud;

    protected Map map;

    public Player() {
    }

    public Player(Map map, int animationLength, int fillOvalX, int fillOvalY, BallController ballController,
                  BatimentBoomcraftController batimentBoomcraftController,
                  BatimentsFarmvilleController batimentsFarmvilleController,
                  VillageFarmVilleController villageFarmVilleController,
                  Hud hud){

        this.map = map;
        this.hud = hud;
        animations = new Animation[animationLength];
        this.fillOvalX = fillOvalX;
        this.fillOvalY = fillOvalY;
        this.ballController = ballController;
        this.batimentBoomcraftController = batimentBoomcraftController;
        this.batimentsFarmvilleController = batimentsFarmvilleController;
        this.villageFarmVilleController = villageFarmVilleController;
    }

    // Méthode permettant de charger l'image du joueur, doit obligatoirement être implémentée
    public abstract void init() throws SlickException;
    protected Animation loadAnimation(SpriteSheet spriteSheet, int startX, int endX, int y) {
        Animation animation = new Animation();
        for (int x = startX; x < endX; x++) {
            animation.addFrame(spriteSheet.getSprite(x, y), 100);
        }
        return animation;
    }
    public void render(Graphics g) {
        g.setColor(new Color(0, 0, 0, .5f));
        g.fillOval((int) x - fillOvalX, (int) y - fillOvalY, 32, 16);
        g.drawAnimation(animations[direction + (isMoving() ? 4 : 0)], (int) x - 32, (int) y - 64);
    }

    public void update(int delta) throws SlickException {
        if (this.isMoving()) {
            updateDirection();
            float futurX = getFuturX(delta);
            float futurY = getFuturY(delta);
            ConnectionHowob.getInstance().sendLocation((int)x, (int)y);

            if(map.getName().contains("resources/map/BoomCraft.tmx")) {
                HashMap<Integer, Batiments> collison = this.batimentBoomcraftController.isCollision((int) x, (int) y);
                Integer key = -1;
                Batiments batiments = null;
                if(collison != null) {
                    for (java.util.Map.Entry<Integer, Batiments> entry : collison.entrySet()) {
                        key = entry.getKey();
                        batiments = entry.getValue();
                    }
                    if (key >= 0) {
                        if (batiments instanceof BatimentsBoomCraftAllie) {
                            this.map.changeMap("resources/map/Interior1.tmx");
                            this.x = 150;
                            this.y = 150;
                            futurX = getFuturX(delta);
                            futurY = getFuturY(delta);
                        } else {
                            this.map.changeMap("resources/map/Interior3.tmx");
                            this.x = 150;
                            this.y = 500;
                            futurX = getFuturX(delta);
                            futurY = getFuturY(delta);
                        }
                        batimentIn = batiments;
                        ConnectionHowob.getInstance().sendLocation("BoomCraft", String.valueOf(batiments.getId()));
                    }
                }
            }

            if(map.getName().contains("resources/map/MainMap.tmx")) {
                int[] tabresult = this.ballController.isCollision((int) futurX, (int) futurY);
                if (mania < 101)
                    mania += tabresult[0];
                if (experience < 101)
                    experience += tabresult[1];
                if (life < 101)
                    life += tabresult[5];
                potion += tabresult[2];
                steel += tabresult[3];
                wood += tabresult[4];
            }

            if(map.getName().contains("resources/map/AllFarmVille.tmx")) {
                HashMap<Integer, Batiments> collison = this.villageFarmVilleController.isCollision((int) x, (int) y);
                Integer key = -1;
                Batiments batiments = null;
                if(collison != null) {
                    for (java.util.Map.Entry<Integer, Batiments> entry : collison.entrySet()) {
                        key = entry.getKey();
                        batiments = entry.getValue();
                    }
                    if (key >= 0) {
                        if(batiments != null) {
                            this.x = 150;
                            this.y = 150;
                            futurX = getFuturX(delta);
                            futurY = getFuturY(delta);
                            this.map.changeMap("resources/map/farmVille.tmx");
                            this.hud.changeImage("farmVille.png");
                            ConnectionHowob.getInstance().sendLocation("FarmVille", String.valueOf(collison));
                            batimentsFarmvilleController.setBatimentsFarmVilles(ConnectionFarmVille.getInstance().getBatiments(batiments.getPseudo()));
                            batimentsFarmvilleController.CreateBatiments("resources/map/farmVille.tmx");

                        }
                    }
                }
            }

            boolean collisionMap = this.map.isCollision(futurX, futurY);
            hud.updatePlayer((int)x,(int) y);

            if (collisionMap) {
                stopMoving();
            } else {
                this.x = futurX;
                this.y = futurY;
            }
        }
        updateHud();
    }

    protected void updateHud()
    {
        this.hud.setLIFE_BAR_RATIO((float) life/100);
        this.hud.setMANA_BAR_RATIO((float) mania / 100);
        this.hud.setXP_BAR_RATIO((float) experience / 100);
        this.hud.setNB_POTION(potion);
        this.hud.setNB_WOOD(wood);
        this.hud.setNB_STEEL(steel);
    }

    protected float getFuturX(int delta) {
        return this.x + .15f * delta * dx;
    }

    protected float getFuturY(int delta) {
        float futurY = this.y + .15f * delta * dy;
        if (this.onStair) {
            futurY = futurY - .15f * dx * delta;
        }
        return futurY;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    /**
     * mise à jour de la direction en fonction du vecteur de déplacement
     */
    protected void updateDirection() {
        if (dx > 0 && dx >= Math.abs(dy)) {
            direction = 3;
        } else if (dx < 0 && -dx >= Math.abs(dy)) {
            direction = 1;
        } else if (dy < 0) {
            direction = 0;
        } else if (dy > 0) {
            direction = 2;
        }
    }

    public void setDirection(int direction) {
        switch (direction) {
            case 0:
                dx = 0;
                dy = -1;
                break;
            case 1:
                dx = -1;
                dy = 0;
                break;
            case 2:
                dx = 0;
                dy = 1;
                break;
            case 3:
                dx = 1;
                dy = 0;
                break;
            default:
                dx = 0;
                dy = 0;
                break;
        }
    }

    public boolean isMoving() {
        return dx != 0 || dy != 0;
    }

    public void stopMoving() {
        dx = 0;
        dy = 0;
    }

    public boolean isOnStair() {
        return onStair;
    }

    public void setOnStair(boolean onStair) {
        this.onStair = onStair;
    }

    public void setDx(float dx) {
        this.dx = dx;
    }

    public void setDy(float dy) {
        this.dy = dy;
    }

    public Map getMap() {
        return map;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }
    public void setMania(int mania) {
        this.mania = mania;
    }
    public void setLife(int life) {
        if(life > 100){
            this.life = 100;
        }else
        this.life = life;
    }

    public BallController getBallController() {
        return ballController;
    }

    public void setBallController(BallController ballController) {
        this.ballController = ballController;
    }

    public BatimentBoomcraftController getBatimentBoomcraftController() {
        return batimentBoomcraftController;
    }

    public void setBatimentBoomcraftController(BatimentBoomcraftController batimentBoomcraftController) {
        this.batimentBoomcraftController = batimentBoomcraftController;
    }

    public BatimentsFarmvilleController getBatimentsFarmvilleController() {
        return batimentsFarmvilleController;
    }

    public VillageFarmVilleController getVillageFarmVilleController() {
        return villageFarmVilleController;
    }

    public int getKeyx() {
        return keyx;
    }

    public void setKeyx(int keyx) {
        this.keyx = keyx;
    }

    public static int getExperience() {
        return experience;
    }

    public static int getMania() {
        return mania;
    }

    public static int getLife() {
        return life;
    }

    public static int getPotion() {
        return potion;
    }

    public static void setPotion(int potion) {
        Player.potion = potion;
    }

    public static int getWood() {
        return wood;
    }

    public static void setWood(int wood) {
        Player.wood = wood;
    }

    public static int getSteel() {
        return steel;
    }

    public static void setSteel(int steel) {
        Player.steel = steel;
    }

    public Batiments getBatimentIn() {
        return batimentIn;
    }

    public void setBatimentIn(Batiments batimentIn) {
        this.batimentIn = batimentIn;
    }
}
