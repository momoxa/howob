package be.masi.player;

import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;

/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public class PlayerController implements KeyListener{

    private Player player;
    private Input input;
    private boolean xPressed = false;

    public PlayerController(Player player) {
        this.player = player;
    }

    public void update() {
        if (input.getControllerCount() > 0) {
            try {
                player.setDx(input.getAxisValue(0, 1));
                player.setDy(input.getAxisValue(0, 2));
            }
            catch (Exception e)
            {
               // e.printStackTrace();
            }
        }
    }

    @Override
    public void setInput(Input input) {
        this.input = input;
    }

    @Override
    public boolean isAcceptingInput() {
        return true;
    }

    @Override
    public void inputEnded() {
    }

    @Override
    public void inputStarted() {

    }

    @Override
    public void keyPressed(int key, char c) {
        switch (key) {
            case Input.KEY_UP:
                this.player.setDy(- (1 + player.getMania()/60));
                break;
            case Input.KEY_LEFT:
                this.player.setDx(- (1 + player.getMania()/60));
                break;
            case Input.KEY_DOWN:
                this.player.setDy(1 + player.getMania()/60);
                break;
            case Input.KEY_RIGHT:
                this.player.setDx(1 + player.getMania()/60);
                break;
            case Input.KEY_X : {
                if(!xPressed) {
                    this.player.setKeyx(1);
                    this.xPressed = true;
                }
                break;
            }
        }
    }

    @Override
    public void keyReleased(int key, char c) {
        switch (key) {
            case Input.KEY_UP:
            case Input.KEY_DOWN:
                this.player.setDy(0);
                break;
            case Input.KEY_LEFT:
            case Input.KEY_RIGHT:
                this.player.setDx(0);
                break;
            case Input.KEY_X : {
                this.player.setKeyx(0);
                this.xPressed = false;
                break;
            }
        }
    }
}
