package be.masi.player;

import org.newdawn.slick.*;

/**
 * Created by Xavie on 18-11-15.
 */
public class RemotePlayer{
    String pseudo;
    int id, player_id;
    String map, Batiment, character;
    float x, y;

    public RemotePlayer() {

    }

    public RemotePlayer( float x, float y,  String batiment,String map, String pseudo, int id,String character) {
        super();
        this.pseudo = pseudo;
        this.id = id;
        this.x = x;
        this.y = y;
        this.map = map;
        this.Batiment = batiment;
        this.character = character;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getBatiment() {
        return Batiment;
    }

    public void setBatiment(String batiment) {
        Batiment = batiment;
    }

    public String getCarte() {
        return map;
    }
}
