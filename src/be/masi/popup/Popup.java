package be.masi.popup;


import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import java.util.Vector;

/**
 * Created by syber on 14-01-16.
 */
public class Popup {

    private Rectangle rect;
    private int x, y;
    private Vector<String> tabText = new Vector<String>();


    public Popup(int rectX, int rectY, int rectW, int rectH)
    {
        rect= new Rectangle(rectX,rectY,rectW,rectH);

        this.x = rectX;
        this.y = rectY;
    }
    public void addString(String text)
    {
      tabText.clear();
      tabText.add(text);
    }
    public void deleteString()
    {
        tabText.clear();
    }

    public void render(Graphics g) {
        //dessin du popup et des textes
        if(tabText.size() > 0) {
            g.setColor(Color.transparent);
            g.fill(rect);
            g.setColor(Color.transparent);
            g.draw(rect);


            for (int i = 0; i < tabText.size(); i++) {
                g.setColor(Color.white);
                g.drawString(tabText.get(i), x + 5, y + 20 * i);
            }
        }
        }



    public void delete (Graphics g)
    {
        g.clear();
    }

}
