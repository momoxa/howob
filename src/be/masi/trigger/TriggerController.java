package be.masi.trigger;

import be.masi.batiments.BatimentBoomcraftController;
import be.masi.httpconnection.Connection;
import be.masi.httpconnection.ConnectionBoomCraft;
import be.masi.httpconnection.ConnectionHowob;
import be.masi.httpconnection.ConnectionVeggieCrush;
import be.masi.hud.Hud;
import be.masi.map.Map;
import be.masi.player.Player;
import be.masi.popup.Popup;
import org.json.JSONObject;
import org.lwjgl.Sys;
import org.newdawn.slick.SlickException;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;

/**
 * Created by Ghost_Wanted on 02/11/15.
 */
public class TriggerController {

    private Map map;
    private Player player;
    Hud hud;
    Popup p;

    public TriggerController(Map map, Player player, Hud hud, Popup p) {
        this.map = map;
        this.player = player;
        this.hud = hud;
        this.p = p;

    }

    public void update() throws SlickException {
        this.player.setOnStair(false);
        for (int objectID = 0; objectID < this.map.getObjectCount(); objectID++) {
            if (isInTrigger(objectID)) {
                if ("teleport".equals(this.map.getObjectType(objectID))) {
                    this.teleport(objectID);
                } else if ("stair".equals(this.map.getObjectType(objectID))) {
                    this.player.setOnStair(true);
                } else if ("change-map".equals(this.map.getObjectType(objectID))) {
                    this.changeMap(objectID);
                } else if ("action-batiments".equals(this.map.getObjectType(objectID)))
                {
                    this.actionBatiment(objectID);
                }
            }
        }
    }

    private boolean isInTrigger(int id) {
        return this.player.getX() > this.map.getObjectX(id)
                && this.player.getX() < this.map.getObjectX(id) + this.map.getObjectWidth(id)
                && this.player.getY() > this.map.getObjectY(id)
                && this.player.getY() < this.map.getObjectY(id) + this.map.getObjectHeight(id);
    }

    private void teleport(int objectID) {
        this.player.setX(Float.parseFloat(this.map.getObjectProperty(objectID, "dest-x",
                Float.toString(this.player.getX()))));
        this.player.setY(Float.parseFloat(this.map.getObjectProperty(objectID, "dest-y",
                Float.toString(this.player.getY()))));
    }
    private void actionBatiment(int objectID)
    {
        if(map.getName().contains("resources/map/VeggieCrush.tmx")) {
            String action = this.map.getObjectProperty(objectID, "action", "undefined");
            if (action.compareTo("vendre") == 0) {
                if(player.getKeyx() == 1 && player.getPotion() > 0) {
                    if(ConnectionVeggieCrush.getInstance().sendRessources())
                    player.potion = player.potion -1;

                }
            }
            else if (action.compareTo("acheter") == 0) {
                if(player.getKeyx() == 1) {
                    int life = ConnectionVeggieCrush.getInstance().getRessources();
                    if(life > 0) {
                        player.setLife( player.getLife()+ life);
                    }
                }
            }
        }
        else if (map.getName().contains("resources/map/Interior3.tmx") || map.getName().contains("resources/map/Interior1.tmx")){
            p.addString("Vie : " + player.getBatimentIn().getVieNow() + " / " + player.getBatimentIn().getVieMax());
            String action = this.map.getObjectProperty(objectID, "action", "undefined");
            if (action.compareTo("action") == 0) {
                if(player.getKeyx() == 1) {
                    float lifeBatiment = 0;

                    float experience = player.getExperience();
                    float mania = player.getMania();
                    float batimentVieMax = player.getBatimentIn().getVieMax();


                    float magie = (batimentVieMax / 100) * (experience / 4 * mania / 25);

                    if(ConnectionHowob.getInstance().getTeamId() != player.getBatimentIn().getTeamId()) {
                        lifeBatiment = player.getBatimentIn().getVieNow() - magie;
                        if(lifeBatiment < 0){
                            lifeBatiment = 0;
                        }
                    } else if (ConnectionHowob.getInstance().getTeamId() == player.getBatimentIn().getTeamId())
                    {
                        lifeBatiment = player.getBatimentIn().getVieNow() + magie;
                        if(lifeBatiment > player.getBatimentIn().getVieMax()){
                            lifeBatiment = player.getBatimentIn().getVieMax();
                        }
                    }
                    if(ConnectionBoomCraft.getInstance().sendAction(player.getBatimentIn().getId(),lifeBatiment)) {
                        player.getBatimentIn().setVieNow((int)lifeBatiment);
                        if(ConnectionHowob.getInstance().getTeamId() != player.getBatimentIn().getTeamId()) {
                            player.setLife(player.getLife() - 40);
                        }
                        player.setMania(0);
                        p.addString("Vie : " + player.getBatimentIn().getVieNow() + " / " + player.getBatimentIn().getVieMax());

                        // Sortir batiment
                        if(lifeBatiment == 0)
                        {
                            map.changeMap("resources/map/BoomCraft.tmx");
                            p.deleteString();
                            player.setX(300);
                            player.setY(300);
                            player.getBatimentBoomcraftController().getBatimentsBoomcrafts().remove(player.getBatimentIn());
                        }
                        // Game over
                        if(player.getLife() <=0) {
                            player.setLife(100);
                            player.setExperience(1);
                            // changer le map
                            p.deleteString();
                            map.changeMap("resources/map/MainMap.tmx");
                            hud.changeImage("MainMap.png");
                            player.setX(300);
                            player.setY(300);

                        }
                    }
                }
            }
        }
    }

    private void changeMap(int objectID) throws SlickException {
        this.teleport(objectID);
        String newMap = this.map.getObjectProperty(objectID, "dest-map", "undefined");
        if (!"undefined".equals(newMap)) {
            this.map.changeMap("resources/map/" + newMap);
            System.out.print(newMap);
            checkEvents();
        }
    }
    private void checkEvents()
    {
        if(this.map.getName().contains("resources/map/MainMap.tmx")) {
            ConnectionHowob.getInstance().sendLocation("Howob", String.valueOf(-1));
            this.hud.changeImage("MainMap.png");
        }
        else if(this.map.getName().contains("resources/map/Milieu.tmx")) {
            ConnectionHowob.getInstance().sendLocation("Howob", String.valueOf(-1));
            this.hud.changeImage("Milieu.png");
        }
        else if (this.map.getName().contains("resources/map/BoomCraft.tmx")){
            ConnectionHowob.getInstance().sendLocation("BoomCraft", String.valueOf(-1));
            this.hud.changeImage("BoomCraft.png");
            this.hud.updateBatiments(player.getBatimentBoomcraftController().getBatimentsBoomcrafts());
            p.deleteString();
        }
        else if (this.map.getName().contains("resources/map/AllFarmVille.tmx") && this.map.getOldName().contains("Milieu")) {
            ConnectionHowob.getInstance().sendLocation("FarmVille", String.valueOf(-1));
            this.hud.updateBatiments(player.getVillageFarmVilleController().getVillagesFarmVilles());
            this.hud.changeImage("AllFarmVille.png");
        }
        else if (this.map.getName().contains("resources/map/VeggieCrush.tmx") && this.map.getOldName().contains("AllFarmVille")) {
            ConnectionHowob.getInstance().sendLocation("VeggieCrush", String.valueOf(-1));
        }
    }

}
